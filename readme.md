# Objet Papier website

## Micro press label

Objet Papier is a micro press label, we publish our friends' and our work. We are group of four people : Morgane Bartoli, Juliette Bernachot, Ronan Deshaies and Corentin Moussard. You can reach us at [hello@objetpapier.fr](mailto:hello@objetpapier.fr).

## Site
[![Netlify Status](https://api.netlify.com/api/v1/badges/533fdd08-f153-42f1-92a3-d53ea11ce5b0/deploy-status)](https://app.netlify.com/sites/objet-papier/deploys)
This site is built with Jekyll and deployed to Netlify and uses Netlify CMS, code by [Corentin Moussard](https://alune.fr).
