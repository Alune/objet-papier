---
title: Infos
permalink: /infos/
layout: generic-two-column
id: infos
title-fr: Infos
title-en: Infos
left-column:
  - title-fr: Objet Papier
    content-fr: |-
      Label de micro-édition  
      Montreuil, France
    title-en: Objet Papier
    content-en: |-
      Micro-press label
      Montreuil, France
  - title-fr: Contact
    content-fr: "[hello@objetpapier.fr](mailto:hello@objetpapier.fr)"
    title-en: Contact
    content-en: "[hello@objetpapier.fr](mailto:hello@objetpapier.fr)"
  - title-fr: Actus
    content-fr: "[Facebook](https://www.facebook.com/objetpapier/),
      [Instagram](https://www.instagram.com/objet_papier/?hl=fr)"
    title-en: News
    content-en: "[Facebook](https://www.facebook.com/objetpapier/),
      [Instagram](https://www.instagram.com/objet_papier/?hl=fr)"
  - title-fr: Commande et livraison
    content-fr: >-
      Les frais d’envoi sont calculés en fonction du poids de chaque édition, si
      vous souhaitez en commander plusieurs, contactez nous afin de réduire le
      prix final.  


      Pour les commandes internationales, contactez nous.
    title-en: Orders and shipping
    content-en: >-
      Shipping costs are depending on the weight of the package, if you want to
      order multiple items, please contact us to get the shipping amount.  


      For international shipping, please contact us.
right-column:
  - title-fr: Crédits
    content-fr: |-
      Design : Morgane Bartoli et Corentin Moussard\
      Développement : Corentin Moussard\
      Typographies : Madras et Crimson  

      © Objet Papier et tous les contributeurs 2016-2025
    title-en: Credits
    content-en: |-
      Design : Morgane Bartoli and Corentin Moussard\
      Programming : Corentin Moussard\
      Typography : Madras and Crimson  

      © Objet Papier and all it's contributors 2016-2025
---
