---
title: Objet Papier - Contributions
permalink: /contributions/
layout: contributions
id: contributions
title-fr: Amis
title-en: Friends
contributions-list:
  - title: Ronan Deshaies
    role-fr: Co-fondateur, auteur
    role-en: Co-founder, author
    link: http://cargocollective.com/ronandeshaies
  - title: Morgane Bartoli
    role-fr: Co-fondatrice, autrice
    role-en: Co-founder, author
    link: http://morganebartoli.com/
  - title: Corentin Moussard
    role-fr: Developpement web, auteur
    role-en: Web development, author
    link: https://alune.fr/
  - title: Juliette Bernachot
    role-fr: Autrice, fabricante
    role-en: Author, production manager
    link: ""
  - title: Guillaume Rossi
    role-fr: Auteur
    role-en: Author
    link: https://guillaumerossi.fr/
  - title: Mamzelle Heing
    role-fr: Autrice
    role-en: Author
    link: https://heingshop.com/
  - title: Aline Roy
    role-fr: Photographe, retoucheuse
    role-en: Photographer, photo editor
    link: http://aline-roy.com/
  - title: Jérémy Roques
    role-fr: Imprimeur
    role-en: Printer
    link: ""
  - title: Lewiss Le Vice
    role-fr: Imprimeur
    role-en: Printer
    link: ""
  - title: Sixtyne Perez
    role-fr: Autrice
    role-en: Author
    link: ""
  - title: Julien Alonet
    role-fr: Photographe, auteur
    role-en: Photographer, author
    link: https://www.instagram.com/julien.al/
  - title: Manon Gavalda
    role-fr: Designer Objet
    role-en: Product designer
    link: ""
  - title: Ben Zedrine
    role-fr: Auteur
    role-en: Author
  - title: Alessandro Zuffi
    role-fr: Auteur
    role-en: Author
  - title: Gaspard Choron
    link: https://gaspardchoron.fr/
    role-fr: Auteur
    role-en: Author
  - title: Marion Combas
    role-fr: Autrice
    role-en: Author
  - title: Yves Mutant
    role-fr: Auteur
    role-en: Author
  - title: Manon Lestrade
    role-fr: Autrice
    role-en: Author
    link: http://gloriakitsune.com/
  - title: Dimitri Robert
    role-fr: Auteur
    role-en: Author
    link: https://www.instagram.com/hej_malmo
  - title: "@jah_nita"
    link: https://www.instagram.com/jah_nita
    role-fr: Autrice
    role-en: Author
  - title: Estelle Martín Chaves
    role-fr: Autrice
    role-en: Author
    link: https://www.instagram.com/e._mck
  - title: Andrée Ospina
    link: https://leseditionsmaison-maison.tumblr.com/
    role-fr: Autrice
    role-en: Author
  - title: Robin Defilhes
    link: http://defilhes.fr/
    role-fr: Auteur
    role-en: Author
  - title: Hunter Bass
    role-fr: Auteur
    role-en: Author
  - title: Crippy
    link: https://print-it.objetpapier.fr/surprise/
    role-fr: Associé numérique
    role-en: Digital associate
  - title: Marcel Bartoli
    role-fr: Imprimeur 3D
    role-en: 3D Printer
  - title: Sarah Saulnier de Praingy
    link: https://ssdp.fr/
    role-fr: Autrice
    role-en: Author
  - title: Emily Druzinec
    link: https://www.instagram.com/cosmofall/
    role-fr: Autrice
    role-en: Author
  - title: Paul Marique
    link: https://paulmarique.be/
    role-fr: Auteur
    role-en: Author
  - title: Lou Beaumont
    role-fr: Autrice
    role-en: Author
  - title: Joséphine Brueder
    link: https://josephinebrueder.com/
    role-fr: Autrice
    role-en: Author
  - title: Antoine Lefebvre
    link: https://www.antoinelefebvre.net
    role-fr: Auteur
    role-en: Author
---
