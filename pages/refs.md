---
title: Refs
permalink: /refs/
layout: generic-two-column
id: refs
title-fr: Réfs
title-en: Refs
left-column:
  - title-fr: Salons
    content-fr: >-
      * Offprint Paris, Pavillon de l'Arsenal, Nov. 2024

      * Kikk festival, Namur, Oct. 2024

      * Recreo #5, IVAM, Valencia, Oct. 2024

      * Indiecon, Oberhafen, Hambourg, Sept. 2024

      * Trafic #2, FRAC PACA, Marseille, Mars 2024

      * La Bibliothèque, c'est nous, Aux Adelphes, Troyes, Dec. 2023

      * Faites de l’hiver, Halle de la Croix-de-Chavaux, Montreuil, Dec. 2023

      * Grand Print, École supérieure d'art et design TALM, Le Mans, Nov. 2023

      * Recreo #4, IVAM, Valencia, Oct. 2023

      * Faites de l’automne, Halle de la Croix-de-Chavaux, Montreuil, Oct. 2023

      * Le fond du frigo, Pot au Print festival, Colloque/La cité fertile, Pantin, Sept. 2023

      * Césure c’est ouvert !, Césure, Paris, Sept. 2023

      * Joie totale !, Les Amarres, Paris, Sept. 2023

      * UEF#3, La Fanzinothèque, Poitiers, Août 2023

      * Tsundoku, Printworks, Dublin, Juill. 2023

      * ZINEFEST #10, Marché des Douves, Bordeaux, Juill. 2023

      * Faites de l’été, Halle de la Croix-de-Chavaux, Montreuil, Juin 2023

      * La Micro-Machine #8, La Briqueterie, Amiens, Mai 2023

      * Libros Mutantes Art Book Fair, La Casa Encendida, Madrid, Avr. 2023

      * Marché de noël, Quintal, Paris, Déc. 2022

      * Drawing Market, Drawing House, Paris, Dec. 2022

      * Accidents - Atypical Book Fair, Cité internationale des arts, Paris, Nov. 2022

      * The Floto Shop, Floréal Belleville, Paris, Nov. 2022

      * Recreo #3, IVAM, Valencia, Oct. 2022

      * Black Market #3, La Station - Gare des Mines, Paris, Juil. 2022

      * ZINEFEST #9, Marché des Douves, Bordeaux, Juill. 2022

      * Coucou Minou Festival 2, La Briche Foraine, Saint-Denis, Juin 2022

      * The Other Book Fair, Quartair, La Haye, Mai 2022

      * Trafic, FRAC PACA, Marseille, Mai 2022

      * Book Brutality, Syndicat Potentiel, Strasbourg, Déc. 2021

      * Accidents - Atypical Book Fair, La Bellevilloise, Paris, Déc. 2021

      * Offprint Off, Cahier Central, Paris, Nov. 2021

      * Juste une impression, Le Temps Machine, Joué-lès-Tours, Nov. 2021

      * Multiple Art Days, Fondation Fiminco, Romainville, Sept. 2021

      * Wild Zine Saloon, La grande passerelle, Saint-Malo, Aout 2021

      * Black Market #2, La Station - Gare des Mines, Paris, Juil. 2021

      * Fichez-nous la paix !, Place Voltaire, Arles, Juil. 2021

      * Été Carbone - MAXI KIOSQUE, Le Vecteur, Charleroi, Été 2021

      * FILAF, Centro Espagnol, Perpignan, Juin 2021

      * Fanzine Révolution - Livre Objet, Le Pavillon des Canaux, Paris, Oct. 2020

      * Marché de petite-auto-micro-édition #2, Floréal Belleville, Paris, Jan. 2020

      * Impressions Multiples, ÉSAM Caen/Cherbourg, Nov. 2019

      * Festival Microscopies #4, Lille, Oct. 2019

      * Rebel Rebel : salon du fanzine, FRAC PACA, Marseille, Sept. 2019

      * Salon Couper-Décaler, Le Tetris, Le Havre, Mai 2019

      * Les Puces Typo #9, CFI, Bagnolet, Mai 2019

      * Ghent Art Book Fair 2019, Ghent, Mai 2019

      * Grrrlstechzinefair, La Gaîté Lyrique, Paris, Avril 2019

      * Festival 100% Makerz, Villette Makerz, Paris, Mars 2019

      * Papier Carbone festival 3, BPS22, Charleroi, Mars 2019

      * Eat My Paper, Recyclart, Bruxelles, Dec. 2018

      * Fu:bar 2018 (via Random Pixel Order), Akc Medika, Zagreb, Oct. 2018

      * RisoFest Paris (via Riso Friends), Galerie 34Greneta, Paris, Juin 2018

      * Salon de l'auto-édition Anti-Aufklärung / AA4, Point Éphémère, Paris, Mai 2018

      * L’illu a la frite, Point Éphémère, Paris, Mars 2018

      * Salon Microphasme, Rouen, Mars 2018
    title-en: Fairs
    content-en: >-
      * Offprint Paris, Pavillon de l'Arsenal, Nov. 2024

      * Kikk festival, Namur, Oct. 2024

      * Recreo #5, IVAM, Valencia, Oct. 2024

      * Indiecon, Oberhafen, Hamburg, Sept. 2024

      * Trafic #2, FRAC PACA, Marseille, March 2024

      * La Bibliothèque, c'est nous, Aux Adelphes, Troyes, Dec. 2023

      * Faites de l’hiver, Halle de la Croix-de-Chavaux, Montreuil, Dec. 2023

      * Grand Print, École supérieure d'art et design TALM, Le Mans, Nov. 2023

      * Recreo #4, IVAM, Valencia, Oct. 2023

      * Faites de l’automne, Halle de la Croix-de-Chavaux, Montreuil, Oct. 2023

      * Le fond du frigo, Pot au Print festival, Colloque/La cité fertile, Pantin, Sept. 2023

      * Césure c’est ouvert !, Césure, Paris, Sept. 2023

      * Joie totale !, Les Amarres, Paris, Sept. 2023

      * UEF#3, La Fanzinothèque, Poitiers, Aug 2023

      * Tsundoku, Printworks, Dublin, July 2023

      * ZINEFEST #10, Marché des Douves, Bordeaux, July 2023

      * Faites de l’été, Halle de la Croix-de-Chavaux, Montreuil, June 2023

      * La Micro-Machine #8, La Briqueterie, Amiens, May 2023

      * Libros Mutantes Art Book Fair, La Casa Encendida, Madrid, Apr. 2023

      * Marché de noël, Quintal, Paris, Dec 2022

      * Drawing Market, Drawing House, Paris, Dec. 2022

      * Accidents - Atypical Book Fair, Cité internationale des arts, Paris, Nov. 2022

      * The Floto Shop, Floréal Belleville, Paris, Nov. 2022

      * Recreo #3, IVAM, Valencia, Oct. 2022

      * Black Market #3, La Station - Gare des Mines, Paris, July 2022

      * ZINEFEST #9, Marché des Douves, Bordeaux, July 2022

      * Coucou Minou Festival 2, La Briche Foraine, Saint-Denis, June 2022

      * The Other Book Fair, Quartair, The Hague, May 2022

      * Trafic, FRAC PACA, Marseille, May 2022

      * Book Brutality, Syndicat Potentiel, Strasbourg, Dec. 2021

      * Accidents - Atypical Book Fair, La Bellevilloise, Paris, Dec. 2021

      * Offprint Off, Cahier Central, Paris, Nov. 2021

      * Juste une impression, Le Temps Machine, Joué-lès-Tours, Nov. 2021

      * Multiple Art Days, Fondation Fiminco, Romainville, Sept. 2021

      * Wild Zine Saloon, La grande passerelle, Saint-Malo, Aug. 2021

      * Black Market #2, La Station - Gare des Mines, Paris, July 2021

      * Fichez-nous la paix !, Place Voltaire, Arles, July 2021

      * Été Carbone - MAXI KIOSQUE, Le Vecteur, Charleroi, Summer 2021

      * FILAF, Centro Espagnol, Perpignan, June 2021

      * Fanzine Révolution - Livre Objet, Le Pavillon des Canaux, Paris, Oct. 2020

      * Marché de petite-auto-micro-édition #2, Floréal Belleville, Paris, Jan. 2020

      * Impressions Multiples, ÉSAM Caen/Cherbourg, Nov. 2019

      * Festival Microscopies #4, Lille, Oct. 2019

      * Rebel Rebel : salon du fanzine, FRAC PACA, Marseille, Sept. 2019

      * Salon Couper-Décaler, Le Tetris, Le Havre, May 2019

      * Les Puces Typo #9, CFI, Bagnolet, May 2019

      * Ghent Art Book Fair 2019, Ghent, May 2019

      * Grrrlstechzinefair, La Gaîté Lyrique, Paris, April 2019

      * Festival 100% Makerz, Villette Makerz, Paris, March 2019

      * Papier Carbone festival 3, BPS22, Charleroi, March 2019

      * Eat My Paper, Recyclart, Bruxelles, Dec. 2018

      * Fu:bar 2018 (via Random Pixel Order), Akc Medika, Zagreb, Oct. 2018

      * RisoFest Paris (via Riso Friends), Galerie 34Greneta, Paris, June 2018

      * Salon de l'auto-édition Anti-Aufklärung / AA4, Point Éphémère, Paris, May 2018

      * L’illu a la frite, Point Éphémère, Paris, March 2018

      * Salon Microphasme, Rouen, March 2018
  - title-fr: Archivage
    title-en: Archive and Distribution
    content-fr: |-
      * OP001 à 05 Zines of the Zone
      * OP002,03,04,05,07 Fanzinothèque de Poitiers
      * OP002,03,04,05,07,08,09,11 Bibliothèque Esadhar Rouen
      * OP002 Le Sprint
      * OPEXP007 et OPEXP011 Riso Friends
    content-en: |-
      * OP001 to 05 Zines of the Zone
      * OP002,03,04,05,07 Fanzinothèque of Poitiers
      * OP002,03,04,05,07,08,09,11 Library Esadhar Rouen
      * OP002 Le Sprint
      * OPEXP007 and OPEXP011 Riso Friends
right-column:
  - title-fr: Ateliers et Conférences
    content-fr: >-
      * Recreo #5, IVAM, Valencia, Oct. 2024

      * Indiecon, Oberhafen, Hambourg, Sept. 2024

      * 10 ans CNAP/BK, Bibliothèque Kandinsky Centre Pompidou, Paris, Juin 2024

      * PrePostPrint, Césure, Paris, Jan. 2024

      * Fanzines et numérique, UEF#3, La Fanzinothèque, Poitiers, Août 2023

      * Agrafes & Massicot, Livresse Festival, Le Vecteur, Charleroi, Oct. 2022

      * OffprintOff x Objet Papier, Cahier Central, Paris, Nov. 2021

      * PERFORMANCE, lancement, Librairie sans titre, Paris, Nov. 2021

      * Communication et relations affectives à l'ère du numérique, Efet Studio Créa, Paris, Sept. 2021

      * Multiple Art Days, Fondation Fiminco, Romainville, Sept. 2021

      * FILAF, Centro Espagnol, Perpignan, Juin 2021

      * Disons Design, ESAAT Roubaix, Roubaix, Dec. 2019

      * Impressions Multiples (with Back Office), ÉSAM Caen/Cherbourg, Nov. 2019

      * MICRO L̶u̶n̶d̶i̶, Le Pylos, Vitry-sur-Seine, Nov. 2019

      * Ghent Art Book Fair 2019, Ghent, Mai 2019

      * École LISAA, Paris, Mai 2019

      * Le Lieu Multiple, Poitiers, Avril 2019

      * Festival 100% Makerz, Villette Makerz, Paris, Mars 2019

      * Papier Carbone festival 3, BPS22, Charleroi, Mars 2019
    title-en: Workshop and Conferences
    content-en: >-
      * Recreo #5, IVAM, Valencia, Oct. 2024

      * Indiecon, Oberhafen, Hambourg, Sept. 2024

      * 10 years CNAP/BK, Bibliothèque Kandinsky Centre Pompidou, Paris, June 2024

      * PrePostPrint, Césure, Paris, Jan. 2024

      * Fanzines and digital, UEF#3, La Fanzinothèque, Poitiers, Aug. 2023

      * Agrafes & Massicot, Livresse Festival, Le Vecteur, Charleroi, Oct. 2022

      * OffprintOff x Objet Papier, Cahier Central, Paris, Nov. 2021

      * PERFORMANCE, launching, Librairie sans titre, Paris, Nov. 2021

      * Communication and emotional relationships in the digital age, Efet Studio Créa, Paris, Sept. 2021

      * Multiple Art Days, Fondation Fiminco, Romainville, Septembre 2021

      * FILAF, Centro Espagnol, Perpignan, Juin 2021

      * Disons Design, ESAAT Roubaix, Roubaix, Dec. 2019

      * Impressions Multiples (with Back Office), ÉSAM Caen/Cherbourg, Nov. 2019

      * MICRO L̶u̶n̶d̶i̶, Le Pylos, Vitry-sur-Seine, Nov. 2019

      * Ghent Art Book Fair 2019, Ghent, May 2019

      * École LISAA, Paris, May 2019

      * Le Lieu Multiple, Poitiers, April 2019

      * Festival 100% Makerz, Villette Makerz, Paris, March 2019

      * Papier Carbone festival 3, BPS22, Charleroi, March 2019
  - title-fr: Publications
    content-fr: |-
      * Culturplaza, 10/2024
      * Lueur Vive, 10/2023
      * Balises, le magazine de la BPI Pompidou, 01/2023
      * L, Libération, 01/2023
      * Agrafes & Massicot, Livresse Festival 10/2022
      * Point contemporain, 11/2021
      * Les Puces Typographiques #10 en ligne, 06/2020
      * Tévé Carbone - Le live de Papier Carbone, 05/2020
      * Radio Moulin, 10/2019
      * Radio Grenouille, 09/2019
      * Grrrlstechzinefair, 04/2019
      * Grazia, 10/2018
      * Post-Digital Publishing Archive, 02/2018
      * Comme un arbre, 01/2018
      * PPDO, 05/2017
      * Grazia, 02/2017
    title-en: Publications
    content-en: |-
      * Culturplaza, 10/2024
      * Lueur Vive, 10/2023
      * Balises, magazine of BPI Pompidou, 01/2023
      * L, Libération, 01/2023
      * Agrafes & Massicot, Livresse Festival, 10/2022
      * Point contemporain, 11/2021
      * Les Puces Typographiques #10 en ligne, 06/2020
      * Tévé Carbone - Le live de Papier Carbone, 05/2020
      * Radio Moulin, 10/2019
      * Radio Grenouille, 09/2019
      * Grrrlstechzinefair, 04/2019
      * Grazia, 10/2018
      * Post-Digital Publishing Archive, 02/2018
      * Comme un arbre, 01/2018
      * PPDO, 05/2017
      * Grazia, 02/2017
  - title-fr: Librairies
    title-en: Bookstores
    content-fr: Café librairie Michèle Firk, Monte en l’air, Le Pied de Biche,
      Disparate, Motto Berlin, Athenaeum, Yellow Cube Gallery, Section 7,
      Volume, Classic, Librairie sans titre, Cahier Central, Yvon Lambert.
    content-en: Café librairie Michèle Firk, Monte en l’air, Le Pied de Biche,
      Disparate, Motto Berlin, Athenaeum, Yellow Cube Gallery, Section 7,
      Volume, Classic, Librairie sans titre, Cahier Central, Yvont Lambert.
---
