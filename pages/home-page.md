---
title: Objet Papier - Accueil
permalink: /home/
layout: home-page
id: home-page
title-fr: |-
  Label de micro-édition  
  Montreuil, France
title-en: |-
  Micro-press label  
  Montreuil, France
contact: hello@objetpapier.fr
facebook: https://www.facebook.com/objetpapier/
instagram: https://www.instagram.com/objet_papier/
description-fr: Objet Papier est un label de micro-édition montreuillois qui
  publie des ouvrages de photographies, illustrations, montages, mots et
  expérimentations diverses. Collectif composé de Ronan Deshaies, Morgane
  Bartoli, Juliette Bernachot et Corentin Moussard, il tend à proposer des
  cartes blanches graphiques et littéraires en utilisant le médium de l’édition
  papier tout en s’affranchissant des limites traditionnelles de celui-ci. À
  rebours de l’uniformisation numérique et à l’image d’une génération de l’entre
  deux, le label questionne et fait dialoguer les supports.
description-en: "Objet Papier is a micro-press label from Montreuil, France.
  Since 2016, it published numerous photographies, illustrations, words and
  digital experimental works. Members are : Ronan Deshaies, Morgane Bartoli,
  Juliette Bernachot and Corentin Moussard. It gives graphic and literacy free
  hand to artists who are interested in bypassing traditional paper edition. On
  the contrary of digital standardization, the label questions and discusses the
  different media, as a non-native generation does."
---
