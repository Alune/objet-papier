const variableLetters = document.querySelectorAll(".variable");
const bg = document.querySelector(".blue-bg")


// LETTER POSITION BASED

// window.addEventListener("mousemove", function(e) {
//   const xpos = e.clientX;
//   variableLetters.forEach(letter => {
//     const letterPos = letter.getBoundingClientRect();
//     const letterX = letterPos.left;
//     const letterR = letterPos.left + letterPos.width;
//     if ((xpos >= letterX) && (xpos <= letterR)) {
//       letter.style.fontVariationSettings = "\'wdth\' 100";
//     } else {
//       letter.style.fontVariationSettings = "\'wdth\' 900";
//     }
//   });
// })




// SCREEN WIDTH DIVISION BASED

window.addEventListener("mousemove", function(e) {
  const xpos = e.clientX;
  const viewWidth = window.innerWidth;
  const viewMiddle = viewWidth / 2;
  const bgPercentage = - ((xpos - viewMiddle) / (viewWidth * 1.2)) * 100;
  bg.style.transform = "translateX(" + bgPercentage + "%)"

  const letterCount = variableLetters.length;
  const divisionWidth = parseInt(viewWidth / letterCount);
  const halfDivision = divisionWidth / 2;

  variableLetters.forEach(letter => {
    const letterIndex = variableLetters.length - 1 - parseInt(letter.getAttribute("id"));
    const scale = divisionWidth * 7;

    const limitLeft = letterIndex * divisionWidth - scale;
    const limitRight = letterIndex * divisionWidth + divisionWidth + scale;

    let limitMiddle
    if (letterIndex == 0) {
      limitMiddle = letterIndex * divisionWidth;
    }
    else if (letterIndex == (variableLetters.length - 1)) {
      limitMiddle = letterIndex * divisionWidth + divisionWidth;
    }
    else {
      limitMiddle = letterIndex * divisionWidth + halfDivision;
    }
    
    const widthMin = 100;
    const widthMax = 900;

    if ((xpos >= limitLeft) && (xpos <= limitMiddle)) {
      xposZero = Math.abs(xpos - limitMiddle);
      max = limitMiddle - limitLeft;
      
      const widthPercentage = (xposZero / max)*100;
      const rangePercentage = (widthPercentage * (widthMax - widthMin) / 100) + widthMin
      
      letter.style.fontVariationSettings = "\'wdth\' " + rangePercentage;
    } else if ((xpos >= limitMiddle) && (xpos <= limitRight)) {
      xposZero = xpos - limitMiddle;
      max = limitRight - limitMiddle
      
      const widthPercentage = (xposZero / max)*100;
      const rangePercentage = (widthPercentage * (widthMax - widthMin) / 100) + widthMin

      letter.style.fontVariationSettings = "\'wdth\' " + rangePercentage;
    } else {
      letter.style.fontVariationSettings = "\'wdth\' 900";
    }
  });
})