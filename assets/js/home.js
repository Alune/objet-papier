

function displayFooter() {
  const body = document.body;
  const content = document.querySelector(".home-content");
  const footer = document.querySelector(".site-footer");

  const scrollPosition = window.pageYOffset;
  const contentHeight = content.offsetHeight; 
  const footerHeight = footer.offsetHeight;
  const viewHeight = window.innerHeight;

  var totalHeight = (contentHeight - viewHeight + footerHeight/4);


  if(scrollPosition >= totalHeight) {
      body.classList.remove('dark-bg');
      footer.classList.add('visible');
  }

  if(scrollPosition < totalHeight) {
      body.classList.add('dark-bg');
      footer.classList.remove('visible');
  }
}

function hideBaseline() {
  const scrollPosition = window.pageYOffset;
  const baseline = document.querySelector(".baseline");

  if (scrollPosition != 0) {
      baseline.classList.add("hidden");
 } if (scrollPosition == 0) {
     baseline.classList.remove("hidden");
 }
}


function logoScrollAnimation() {
  const logos = document.querySelectorAll('.logo');
  const logoOne = document.querySelector('.logo-one');
  const logoTwo = document.querySelector('.logo-two');
  const logoThree = document.querySelector('.logo-three');
  const logoFour = document.querySelector('.logo-four');
  const logoFive = document.querySelector('.logo-five');
  const logoSix = document.querySelector('.logo-six');

  var scrollPosition = window.pageYOffset;
  var scopeIfMultiple = (scrollPosition % 600);
  var triggerOne = 100;
  var triggerTwo = 200;
  var triggerThree = 300;
  var triggerFour = 400;
  var triggerFive = 500;

  function hideAllOtherLogos() {
      for (let i = 0; i < logos.length; i++) {
          const logo = logos[i];
          logo.style.display = "none";
      }
  }

  if(scopeIfMultiple < triggerOne) {
      hideAllOtherLogos();
      logoOne.style.display = "block";
  }

  if((scopeIfMultiple > triggerOne) && (scopeIfMultiple < triggerTwo)) {
      hideAllOtherLogos();
      logoTwo.style.display = "block";
  }

  if((scopeIfMultiple > triggerTwo) && (scopeIfMultiple < triggerThree)) {
      hideAllOtherLogos();
      logoThree.style.display = "block";
  }

  if((scopeIfMultiple > triggerThree) && (scopeIfMultiple < triggerFour)) {
      hideAllOtherLogos();
      logoFour.style.display = "block";
  }

  if((scopeIfMultiple > triggerFour) && (scopeIfMultiple < triggerFive)) {
      hideAllOtherLogos();
      logoFive.style.display = "block";
  }

  if(scopeIfMultiple > triggerFive) {
      hideAllOtherLogos();
      logoSix.style.display = "block";
  }
}

window.addEventListener('scroll', function() {
  displayFooter();
  hideBaseline();
  logoScrollAnimation();
});



document.addEventListener("DOMContentLoaded", function() {
  displayFooter();
  hideBaseline();
  logoScrollAnimation();
});

