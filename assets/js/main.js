function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function rotate20() {
  const elementsToRotate = document.querySelectorAll('.rotate-20');

  for (let i = 0; i < elementsToRotate.length; i++) {
    const element = elementsToRotate[i];
    const randomValue = getRandomInt(-20, 20)
    element.style.transform = "rotate(" + randomValue + "deg)";
  }
}

function rotate30() {
  const elementsToRotate = document.querySelectorAll('.rotate-30');

  for (let i = 0; i < elementsToRotate.length; i++) {
    const element = elementsToRotate[i];
    const randomValue = getRandomInt(-30, 30)
    element.style.transform = "rotate(" + randomValue + "deg)";
  }
}


document.addEventListener("DOMContentLoaded", function() {
  rotate20();
  rotate30();    
});


const hoverRotateElements = document.querySelectorAll('.rotate-hover');

for (let o = 0; o < hoverRotateElements.length; o++) {
  const element = hoverRotateElements[o];
  element.addEventListener('mouseover', function() {
    const min = element.classList.contains('rotate-20') ? -20 : -30
    const max = element.classList.contains('rotate-20') ? 20 : 30
    const newRandomValue = getRandomInt(min, max);
    element.style.transform = "rotate(" + newRandomValue + "deg)";
  })  
}
