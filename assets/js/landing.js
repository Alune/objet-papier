const logos = document.querySelectorAll(".home-logo");

window.addEventListener("DOMContentLoaded", function() {
  function setNextLogo() {
    const currentLogo = document.querySelector(".visible");
    let currentID = parseInt(currentLogo.getAttribute("id"));
    const nextID = currentID == 6 ? 1 : currentID + 1;
    const nextLogo = document.getElementById(nextID);
    currentLogo.classList.remove("visible")
    nextLogo.classList.add("visible")

    setTimeout(setNextLogo, 500);
  }
  setNextLogo();
})
