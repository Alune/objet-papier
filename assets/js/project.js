function projectScroll() {
    const projectScrollPosition = window.pageYOffset;
    const viewHeight = window.innerHeight;
    
    const body = document.body;
    const content = document.querySelector(".project-page-content");
    const footer = document.querySelector(".project-description-container");
    const backLink = document.querySelector(".back-home-link");

    const contentHeight = content.offsetHeight; 
    const footerHeight = footer.offsetHeight;
  
  // var projectHeight = $('.project-page-content').height() - viewHeight;
  // var totalPrHeight = (projectHeight - 1);

//   var contentHeight = $('.project-page-content').outerHeight(); 
//   var footerHeight = $(".project-description-container").outerHeight();
  var totalHeight = (contentHeight - viewHeight + footerHeight/4);
  

  //var projectHeight = $('.project-page-content').height() - 200;

  if(projectScrollPosition >= totalHeight) {
      body.classList.add('dark-bg');
      footer.classList.add('visible');
      backLink.classList.add('white');
  }

  if(projectScrollPosition < totalHeight) {
    body.classList.remove('dark-bg');
    footer.classList.remove('visible');
    backLink.classList.remove('white');
  }

}


function projectVideo() {
    const projectScrollPosition = window.pageYOffset;
    const videoContainer = document.querySelector(".video-container");
    const videoHeader = document.querySelector(".project-header-video");

    if((videoContainer != null) && (projectScrollPosition > 500)){
        videoContainer.classList.add('hidden');
        videoHeader.classList.remove('white');
    }
  
    if((videoContainer != null) && (projectScrollPosition <= 500)) {
        videoContainer.classList.remove('hidden');
        videoHeader.classList.add('white');
    }
}


document.addEventListener('scroll', function() {
    projectScroll();
    projectVideo();
})

document.addEventListener('DOMContentLoaded', function() {
    projectScroll();
    projectVideo();
})
