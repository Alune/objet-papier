var pilledImages = document.querySelectorAll(".pilled-image");
var imagesContainer = document.querySelector(".exp-images");

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  }

for (let i = 0; i < pilledImages.length; i++) {
    const pilledImage = pilledImages[i];
    pilledImage.style.zIndex = "" + i + "";

    imagesContainer.addEventListener ("click", function(){
        if (i >= pilledImages.length -1) {
            i = 0;
            pilledImage.style.zIndex = "0";
            let newRotation = getRandomInt(-20, 20);
            pilledImage.style.transform = "rotate(" + newRotation + "deg)";
            
        } else {
            i++;
            pilledImage.style.zIndex = "" + i + "";
        }
    });
}; 

const imagesToResize = document.querySelectorAll(".check-if-verical-img");

window.onload = function() {
    for (let i = 0; i < imagesToResize.length; i++) {
        const image = imagesToResize[i];
            const imageHeight = image.clientHeight;
            const imageWidth = image.clientWidth;
            console.log(image.clientHeight, image.clientWidth)
            if (imageHeight >= imageWidth) {
                image.classList.add("vertical-img");
                image.style.width = "50%";
            }
    }
}


// experiments toggle info on mobile

const expInfoToggle = document.querySelector(".toggle-exp-data");
const expInfoContent = document.querySelector(".exp-info-content");

expInfoToggle.addEventListener("click", function(){
    if (expInfoContent.classList.contains("visible")) {
        expInfoContent.classList.remove("visible");
    } else {
        expInfoContent.classList.add("visible");
    }
});