---
title: OP012 Paysages que je trouvais beaux dans la vraie vie mais qu’une fois
  photographiés je trouve affreusement banals.
layout: project
number: 12
draft: false
auteur:
  - name: Ronan Deshaies
    website: https://cargocollective.com/ronandeshaies
name: Paysages que je trouvais beaux dans la vraie vie mais qu’une fois
  photographiés je trouve affreusement banals.
date-str: 05/2022
black-cover-image: /assets/uploads/op012-black.jpg
white-cover-image: /assets/uploads/op012-white.jpg
images:
  - /assets/uploads/op012-black.jpg
  - /assets/uploads/op012-01.jpg
  - /assets/uploads/op012-02.jpg
  - /assets/uploads/op012-03.jpg
  - /assets/uploads/op012-04.jpg
fr:
  format: 11x16cm
  nb-pages: "36"
  reliure: Piqûre à cheval & colle
  impression: Numérique quadri & noir seul
  exemplaires: "35"
  description: Paysages que je trouvais beaux dans la vraie vie mais qu’une fois
    photographiés je trouve affreusement banals est le premier numéro de la
    collection.collection éditée par Objet Papier. La collection.collection
    explore les recoins de nos environnements quotidiens, en dévoilant les
    traces retrouvées sur nos extensions de mémoire numérique.
en:
  format: 11x16cm
  nb-pages: "36"
  reliure: Saddle stitched & glue
  impression: Quadricolor digital
  exemplaires: "35"
  description: Landscapes I thought were pretty in real life, but once
    photographed I find terribly common. First issue of collection.collection,
    published by Objet Papier. The collection.collection explores our daily
    environments, revealing lost and found parts of our digital memory
    extensions.
video:
  has-video: false
price:
  is-soldout: true
  amount: 9
  shipping: 2.32
---
