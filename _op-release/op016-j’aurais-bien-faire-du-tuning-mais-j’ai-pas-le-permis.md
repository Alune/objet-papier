---
title: OP016 J’aurais bien fait du tuning mais j’ai pas le permis.
layout: project
number: 16
draft: false
auteur:
  - name: Corentin Moussard
    website: https://alune.fr/
name: J’aurais bien fait du tuning mais j’ai pas le permis.
date-str: 10/2022
black-cover-image: /assets/uploads/op016-1.jpg
white-cover-image: /assets/uploads/op016-cover-white.jpg
images:
  - /assets/uploads/op016-1.jpg
  - /assets/uploads/op016-2.jpg
  - /assets/uploads/op016-3.jpg
  - /assets/uploads/op016-4.jpg
  - /assets/uploads/op016-5.jpg
fr:
  format: 11x16cm
  nb-pages: "24"
  reliure: Piqûre à cheval & colle
  impression: Numérique quadri & noir seul
  exemplaires: "35"
  description: J’aurais bien faire du tuning mais j’ai pas le permis est le
    cinquième numéro de la collection.collection éditée par Objet Papier. La
    collection.collection explore les recoins de nos environnements quotidiens,
    en dévoilant les traces retrouvées sur nos extensions de mémoire numérique.
en:
  format: 11x16cm
  nb-pages: "24"
  reliure: Saddle stitched & glue
  impression: Quadricolor digital
  exemplaires: "35"
  description: I would have done tuning, but I don’t have a license. Fifth issue
    of collection.collection, published by Objet Papier. The
    collection.collection explores our daily environments, revealing lost and
    found parts of our digital memory extensions.
video:
  has-video: false
price:
  is-soldout: true
  amount: 9
  shipping: 2.32
---
