---
title: OP003 La numérisation tue l'image
layout: project
number: 3
draft: false
auteur:
  - name: Morgane Bartoli
    website: http://morganebartoli.com
name: La numérisation tue l'image
date-str: 05/2017
black-cover-image: /assets/uploads/op003-cover-black.jpg
white-cover-image: /assets/uploads/op003-cover-white.jpg
images:
  - /assets/uploads/op003-1.jpg
  - /assets/uploads/op003-2.jpg
  - /assets/uploads/op003-3.jpg
  - /assets/uploads/op003-4.jpg
  - /assets/uploads/op003-5.jpg
fr:
  format: 100 x 160 mm
  nb-pages: "44"
  reliure: Dos carré collé
  impression: Numérique quadri
  exemplaires: "50"
  description: "Troisième parution du label, *La numérisation tue l'image*
    questionne notre rapport à la technologie et au matériel de reproduction.
    Elle exhume et détourne ce lointain souvenir pictural, cet avertissement
    “Danger : le photocopillage tue le livre” que l’on pensait confiné aux
    salles de classes et autres centres de documentation. Par la démonstration
    d’une quarantaine de scans et la destruction partielle d'une image,
    l'édition impose une réflexion sur l’évolution des méthodes de publication,
    du livre imprimé à l’écran."
en:
  format: 100 x 160 mm
  nb-pages: "44"
  reliure: Perfect bound
  impression: Quadricolor digital
  exemplaires: "50"
  description: "Third issue of the label, *La numérisation tue l'image*
    (\"Numerisation kills the image\") questions our relationship between
    technology and reproduction materials. It exhumes and gives another meaning
    of this distant childhood pictorial memory, this warning “Danger: le
    photocopillage tue le livre” (“Danger: photocopying kills the book”) that we
    thought restricted to classrooms and documentation centers. Through 40 scans
    and the partial destruction of an image, the publication imposes a
    discussion on the evolution of publishing methods, from the printed book to
    the screen."
video:
  has-video: false
  video: ""
  video-color: ""
price:
  amount: 5
  shipping: 1.94
  is-soldout: true
---
