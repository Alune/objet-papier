---
title: OP024 Encore une fois, mon téléphone a mieux vu le concert que moi.
layout: project
number: 24
draft: false
auteur:
  - name: Lou Beaumont
name: Encore une fois, mon téléphone a mieux vu le concert que moi.
date-str: 04/2023
black-cover-image: /assets/uploads/op024-1.jpg
white-cover-image: /assets/uploads/op024-cover-white-bidouille.jpg
images:
  - /assets/uploads/op024-1.jpg
  - /assets/uploads/op024-2.jpg
  - /assets/uploads/op024-3.jpg
  - /assets/uploads/op024-4.jpg
  - /assets/uploads/op024-5.jpg
  - /assets/uploads/op024-6.jpg
fr:
  format: 11x16cm
  nb-pages: "24"
  reliure: Piqûre à cheval & colle
  impression: Numérique quadri & noir seul
  exemplaires: "50"
  description: Encore une fois, mon téléphone a mieux vu le concert que moi est le
    onzième numéro de la collection.collection éditée par Objet Papier. La
    collection.collection explore les recoins de nos environnements quotidiens,
    en dévoilant les traces retrouvées sur nos extensions de mémoire numérique.
en:
  format: 11x16cm
  nb-pages: "24"
  reliure: Saddle stitched & glue
  impression: Quadricolor digital
  exemplaires: "50"
  description: >-
    Once again, my phone saw the concert better than I did.\

    Eleventh issue of collection.collection, published by Objet Papier. The collection.collection explores our daily environments, revealing lost and found parts of our digital memory extensions.
video:
  has-video: false
price:
  is-soldout: false
  amount: 9
  shipping: 2.32
---
