---
title: OP009 PERFORMANCE
layout: project
number: 9
draft: false
auteur:
  - name: Various artists
name: PERFORMANCE
date-str: 06/2021
black-cover-image: /assets/uploads/op009-cover-black.jpg
white-cover-image: /assets/uploads/op009-cover-white.jpg
images:
  - /assets/uploads/op009-15.jpg
  - /assets/uploads/op009-0-body.jpg
  - /assets/uploads/op009-2.jpg
  - /assets/uploads/op009-3.jpg
  - /assets/uploads/op009-4.jpg
  - /assets/uploads/op009-5.jpg
  - /assets/uploads/op009-0-motor.jpg
  - /assets/uploads/op009-7.jpg
  - /assets/uploads/op009-8.jpg
  - /assets/uploads/op009-10.jpg
  - /assets/uploads/op009-11.jpg
  - /assets/uploads/op009-13.jpg
  - /assets/uploads/op009-16.jpg
  - /assets/uploads/op009-17.jpg
fr:
  impression: "Numérique quadri "
  nb-pages: 104 + 48
  format: 21 x 28 cm
  reliure: "Élastique "
  exemplaires: " 200"
  description: >-
    Pour sa première publication collective, Objet Papier invite ses amis
    artistes à livrer leur définition de la performance à travers deux angles :
    le corps et le moteur. Reflet d’une époque vouée à la compétition jusqu’à
    l’absurde, la confrontation de l’homme et de la machine semble l’ultime
    limite à explorer. OP009 : PERFORMANCE compile 19 interprétations
    artistiques multi-médiums du grand antagonisme entre muscles et pistons. Une
    double édition accompagnée de deux livrets-texte bilingues prolongeant
    chaque œuvre de manière expérimentale. Une version deluxe incluant une
    écharpe de supporter designée aux couleurs de l’édition est disponible,
    ainsi qu’un site web évolutif dédié :


    [bodyPERFORMANCEmotor](https://performance.objetpapier.fr/)


    Par Ben Zedrine, Alessandro Zuffi, Juliette Bernachot, Julien Al., Gaspard Choron, Marion Combas, Yves Mutant, Manon Lestrade, Dimitri Robert, Ronan Deshaies, @jah_nita, Guillaume Rossi, Estelle Martín Chaves, Corentin Moussard, Les éditions maison-maison, Robin Defilhes, Hunter Bass.
en:
  description: >-
    For its first collective publication, Objet Papier invites its artist
    friends to deliver their definition of performance through two points of
    view: body and motor. As the result of an era obsessed with competition as
    hell, the confrontation between man and machine seems to be the last limit
    to be explored. OP009 : PERFORMANCE compiles 19 artistic multi-medium
    interpretations of the great antagonism between muscles and pistons. A
    double edition accompanied by two bilingual text booklets extending each
    work in an experimental way. A deluxe version including a fan's scarf
    designed with the colors of the edition is available (on demand), as well as
    a dedicated progressive website:


    [bodyPERFORMANCEmotor](https://performance.objetpapier.fr/)


    By Ben Zedrine, Alessandro Zuffi, Juliette Bernachot, Julien Al., Gaspard Choron, Marion Combas, Yves Mutant, Manon Lestrade, Dimitri Robert, Ronan Deshaies, @jah_nita, Guillaume Rossi, Estelle Martín Chaves, Corentin Moussard, Les éditions maison-maison, Robin Defilhes, Hunter Bass
  format: 21 x 28 cm
  nb-pages: 104 + 48
  exemplaires: "200"
  impression: Quadricolor digital
  reliure: "Elastic "
video:
  has-video: false
price:
  is-soldout: false
  amount: 18
  shipping: 6
---
