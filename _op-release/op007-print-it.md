---
title: OP007 Print-it
layout: project
number: 7
draft: false
auteur:
  - name: Morgane Bartoli
    website: http://morganebartoli.com/
  - name: Corentin Moussard
    website: https://alune.fr/
name: Print-it, à la découverte d’Internet !
date-str: 03/2019
black-cover-image: /assets/uploads/op007-cover-black.jpg
white-cover-image: /assets/uploads/op007-cover-white.jpg
images:
  - /assets/uploads/op007-1.jpg
  - /assets/uploads/op007-2.jpg
  - /assets/uploads/op007-3.jpg
  - /assets/uploads/op007-4.jpg
  - /assets/uploads/op007-5.jpg
  - /assets/uploads/op007-6.jpg
fr:
  format: A4 en kit, A5 relié
  nb-pages: 32
  reliure: Baguette
  impression: Maison noir seul
  exemplaires: 7 x 42
  description: >-
    Conçu comme le paroxysme de notre ligne éditoriale qui consiste à explorer
    les liens existant entre papier et numérique, cette septième publication
    s’approprie la technique du CSS-Print permettant de coder un livre ou
    d’imprimer un site. Un outil secret réconciliant le papier et l’écran pour
    célébrer dignement les 30 ans du World Wide Web. *OP007 : Print-it, à la
    découverte d’Internet !* propose au lecteur de devenir acteur en imprimant
    et reliant lui-même son magazine avec l’aide d’un kit et d’une notice à
    partir d’une page web. Truffé de références au monde numérique tant sur la
    forme que sur le fond, tout en s’amusant de codes empruntés à la presse
    jeunesse, cet Objet Codé Non Identifié imprimé maison propose une expérience
    unique à la croisée des savoirs. Snap-it, la police de caractère dessinée
    pour cette édition est open source, téléchargez la
    [ici](https://gitlab.com/Alune/snap-it-mono-font) et trouvez son spécimen
    web [ici](https://objetpapier.fr/snap-it/). 


    [C'EST PARTI !](https://objetpapier.fr/print-it)
en:
  format: A4 in kit, A5 bound
  nb-pages: 32
  reliure: Plastic rod
  impression: Home-made black
  exemplaires: 7 x 42
  description: >-
    Conceived as the climax of our philosophy : exploring the interactions
    between paper and digital, our seventh publication is built upon the
    technique of CSS-Print, allowing us to code a book or to print a website. A
    secret tool that reconciles the paper and the screen in order to celebrate
    the 30th anniversary of the World Wide Web in a very special way. *OP007:
    Print-it, let’s discover the Internet!* gives the reader the opportunity to
    become an actor in the making of this publication, by printing and binding
    his own magazine with the help of a kit and a user’s manual directly from a
    web page. Full of digital world references both formally and fundamentally,
    while having fun with the use of youth press codes, this Unidentified Coded
    Object printed at home offers a unique experience at the crossroads of
    knowledges. Snap-it, the typeface designed for this publication is open
    source, download it [here](https://gitlab.com/Alune/snap-it-mono-font) and
    view the web specimen [here]({{site.baseurl}}/snap-it/).


    [LET'S GO !](https://objetpapier.fr/print-it)
video:
  has-video: false
  video: ""
  video-color: ""
price:
  amount: 4.99
  shipping: 1.94
  is-soldout: true
---
