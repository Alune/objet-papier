---
title: OP002 Blurring Policies
layout: project
number: 2
draft: false
auteur:
  - name: Ronan Deshaies
    website: http://cargocollective.com/ronandeshaies
name: Blurring Policies
date-str: 01/2017
black-cover-image: /assets/uploads/op002-cover-black.jpg
white-cover-image: /assets/uploads/op002-cover-white.jpg
images:
  - /assets/uploads/op002-1.jpg
  - /assets/uploads/op002-2.jpg
  - /assets/uploads/op002-3.jpg
  - /assets/uploads/op002-4.jpg
fr:
  format: A5
  nb-pages: "20"
  reliure: Piqûre à cheval
  impression: Numérique noir
  exemplaires: 110 numérotés
  description: La deuxième parution du label Objet Papier retranscrit des
    rencontres IRL de personnes sans abris à Paris en photographies Google
    Street View retravaillées. En interrogeant la politique de floutage de
    Google, le fanzine aborde la question de la représentation de la misère au
    sein des grands espaces urbains et comment un logiciel de cartographie peut
    inverser les rôles de mobilité et d’invisibilité des personnes intégrées au
    système capitaliste.
en:
  format: A5
  nb-pages: "20"
  reliure: Saddle stitched
  impression: Black digital print
  exemplaires: 110 hand numbered
  description: The second issue of the new French label Objet Papier turns IRL
    meetings with Parisian homeless people to reworked google street view
    photographs. This edition questions Google’s blurring policies and talks
    about the representation of misery in large urban areas and shows how a
    cartography program can reverse the roles in terms of mobility and
    invisibility of people who belong to a capitalist society.
video:
  has-video: false
  video: ""
  video-color: ""
price:
  amount: 4
  shipping: 2.32
  is-soldout: false
---
