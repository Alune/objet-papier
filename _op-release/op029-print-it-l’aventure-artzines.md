---
title: OP029 Print-it, l’aventure ARTZINES !
layout: project
number: 29
draft: false
auteur:
  - name: Antoine Lefebvre
    website: https://www.antoinelefebvre.net
name: Print-it, l’aventure ARTZINES !
date-str: 02/2025
black-cover-image: /assets/uploads/op029-cover-black.jpg
white-cover-image: /assets/uploads/op029-cover-white.jpg
images:
  - /assets/uploads/op029-2.jpg
  - /assets/uploads/op029-3.jpg
  - /assets/uploads/op029-4.jpg
  - /assets/uploads/op029-5.jpg
  - /assets/uploads/op029-6.jpg
  - /assets/uploads/op029-7.jpg
  - /assets/uploads/op029-8.jpg
fr:
  format: A4
  nb-pages: NC
  reliure: Rivets
  impression: Riso & Maison noir seul
  exemplaires: NC
  description: >-
    Un zine à imprimer soi-même !\

    Print-it, l’aventure ARTZINES !, l’aboutissement du projet ARTZINES, utilise la technique du CSS Print pour offrir une expérience unique et personnalisée au lecteur.\

    Après avoir répondu aux questions de l’ordinateur\* à propos de vos propres goûts en matière d’art et de fanzines, ce dernier va générer un livre entièrement codé en piochant dans la grande base de données du magazine ARTZINES. Vous devrez ensuite imprimer\*\*, plier et relier\*\*\* votre propre exemplaire de ce livre pas comme les autres.


    \*Lien fourni à l'achat\

    \*\* L'impression à domicile nécessite une simple imprimante de bureau noir Recto / Verso\

    \*\**Matériel fourni à l'achat


    \----


    Après avoir terminé un doctorat sur l'édition en tant que pratique artistique alternative en 2014, Antoine Lefebvre rencontre un homme mystère qui l'encourage à suivre une piste découverte dans sa thèse. Saisissant cette opportunité, il commence à voyager dans des salons d’éditions à travers le monde pour chercher des zines et interviewer les artistes qui les produisent. Au fil du projet, il se rend compte que les artistes ne sont pas les seul·e·s à avoir quelque chose à dire sur les zines et que d'autres personnes s'en occupent et organisent leurs conditions d'existence. Ce sont les librairies qui les vendent, les organisateur·rice·s de festivals de zines et de salons d’édition, les chercheur·euse·s qui les étudient, les collectionneur·euse·s et les bibliothécaires qui les archivent, quand ce ne sont pas des gens qui font tout ça à la fois, poussés par une éthique Do It Yourself héritée des punks.


    Après une décennie de bouleversements dans ses recherches, Lefebvre avait accumulé une énorme quantité de données et cherchait un moyen d'en faire un livre aussi passionnant qu'un roman policier, tant par son contenu que par sa forme. Il a travaillé avec le collectif Objet Papier pour créer une expérience de lecture unique, une publication de recherche où vous le suivrez dans ses aventures, à la manière d'un « livre dont vous êtes le héros ». Vos choix, aidés par un algorithme génératif, construiront une expérience sur mesure, un livre unique et différent à chaque fois.


    Ce troisième numéro de la revue générative Print-it utilise une nouvelle fois la technique du CSS print qui permet de mettre en page une publication imprimée en utilisant des outils web comme on le ferait pour une page Internet. Pour Print- it, l'aventure ARTZINES !, il s’agit d’explorer les possibilités de cette technique pour créer une publication dont le contenu ainsi que le graphisme varient à chaque fois. En s’associant avec Antoine Lefebvre, Objet Papier propose une anthologie ARTZINES dont chaque exemplaire sera différent de celui d’avant.\

    [www.artzines.info](http://www.artzines.info)\

    \

    Avec le soutien à l'édition du Centre national des arts plastiques.
en:
  format: A4
  nb-pages: N/A
  reliure: Rivets
  impression: Riso & Home-made black
  exemplaires: N/A
  description: >-
    A zine that you have to print yourself!\

    Print-it, ARTZINES Adventures!, the final piece from ARTZINES project, is using the CSS Print technique to offer a unique and personalized experience to the audience. After answering the computer's\* questions about your own tastes in fanzines and cultural scene, it will generate a whole coded book by taking content from the complete database of ARTZINES magazine. Then you will have to print\*\*, fold and bind\*\*\* your copy of a book which is different every time.


    \*Link provided with purchase\

    \*\* Home printing requires a simple black Recto Verso desktop printer\

    \*\**Hardware provided with purchase


    \----


    After completing a PhD about publishing as an alternative artistic practice in 2014, Lefebvre met a mystery man who encouraged him to follow a lead discovered in his dissertation. Seizing this opportunity, Lefebvre started traveling to art book fairs across the globe looking for zines and interviewing the artists who make them. As the project went on, he came to think that artists are not the only ones who have something to say about zines and that other people care for zines and organize their conditions of existence. They are the bookshop owners, the zine fest and art book fair organizers, the zine study researchers, the collectors and librarians, when they are not doing all of that at the same time pushed by a Do It Yourself ethos inherited from punks.


    After a decade of research upheavals, Lefebvre had amassed an enormous quantity of data and was looking for a way to turn it into a book that would be as captivating as a mystery novel, both in content and form. He worked with the collective Objet Papier to create a one-of-a-kind reading experience, a Choose Your Adventure research publication that embarks the reader on his adventures, creating a tailored experience unique for each reader.


    This third issue once again uses the CSS print technique which consist in creating a printed layout using web languages and tools in the same way as for a web page. For Print-it, ARTZINES adventures!, they explored the possibilities of this technique even further to create a publication whose content, as well as graphics, vary each time it is generated to be printed. Objet Papier has teamed up with Antoine Lefebvre to produce an ARTZINES anthology, in which each copy  is different from the other.
video:
  has-video: false
price:
  is-soldout: false
  amount: 20
  shipping: 6
---
