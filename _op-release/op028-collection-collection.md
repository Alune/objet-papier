---
title: OP028 collection.collection
layout: project
number: 28
draft: false
auteur:
  - name: Objet Papier
name: collection.collection
date-str: 11/2023
black-cover-image: /assets/uploads/op018-0.jpg
white-cover-image: /assets/uploads/op018-0blanc.jpg
images:
  - /assets/uploads/pop018-1.jpg
  - /assets/uploads/pop018-2.jpg
  - /assets/uploads/pop018-3.jpg
  - /assets/uploads/pop018-4.jpg
  - /assets/uploads/pop018-5.jpg
fr:
  format: A6
  nb-pages: "372"
  reliure: Pliage, colle & piqûre
  impression: Photocopieur, numérique & tirage photo
  exemplaires: "5"
  description: >-
    Durant plus d’un an, nous avons proposé à des auteur·ices de plonger dans
    leurs archives de photos de téléphone à la recherche d’une obsession qu’iels
    auraient inconsciemment développé. Ce pack final regroupe les douze
    publications dans une boîte fait-main, un texte explicatif et trois tirages
    photographiques.


    “Un manifeste de la non-photographie exotisant le réel, qui revêt tout son sens une fois compilé, toutes couleurs mélangées. La vie quotidienne à travers un kaléidoscope.”
en:
  format: A6
  nb-pages: "372"
  reliure: Fold, glued & saddle stitched
  impression: Copy-machine, digital & photographic print
  exemplaires: "5"
  description: >-
    For more than a year, we asked authors to search into their phone pictures
    archive to find an obsession that they had unconsciously developed. This
    final pack brings together the twelve publications in a handmade box, a
    special text and three photographic prints.


    “A manifesto of non-photography exoticizing reality, which takes on its full meaning once compiled, all colors mixed. Daily life through a kaleidoscope.”
video:
  has-video: false
price:
  is-soldout: false
  amount: 95
  shipping: 8.2
---
