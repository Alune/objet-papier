---
title: OP001 Objet Papier
layout: project
number: 1
draft: false
auteur:
  - name: Ronan Deshaies
    website: http://cargocollective.com/ronandeshaies
name: Objet Papier
date-str: 07/2016
black-cover-image: /assets/uploads/op001-cover-black.jpg
white-cover-image: /assets/uploads/op001-cover-white.jpg
images:
  - /assets/uploads/op001-1.jpg
  - /assets/uploads/op001-2.jpg
  - /assets/uploads/op001-3.jpg
  - /assets/uploads/op001-4.jpg
  - /assets/uploads/op001-5.jpg
fr:
  format: A5
  nb-pages: 24 + 1 feuillet
  reliure: Rivets
  impression: Numérique quadri
  exemplaires: 50 numérotés
  description: >-
    *Objet Papier*, première publication du label, constitue à la fois la
    conclusion et la synthèse de la trilogie initiée par les pièces
    *[Rejet / Rejection](http://rejet-rejection.tumblr.com/)* et *[Et les
    mouettes…](https://vimeo.com/162227173)* Tandis que la première création
    vidéo avait pour sujets l'intégration en milieu urbain, l'adaptation à un
    environnement étranger et la difficulté à faire corps avec une communauté
    donnée, la seconde abordait la réappropriation et la reconstruction
    psychologique dans un Paris post-attentats.

    Conséquence directe d'un projet de magazine avorté, *Objet Papier*  est le fruit d'un travail commun portant sur les obsessions toxiques, interrogeant la cohérence de la presse papier et revisitant les thématiques précédentes.
en:
  format: A5
  nb-pages: 24 + 1 leaflet
  reliure: Rivets
  impression: Quadricolor digital print
  exemplaires: 50 hand numbered
  description: "*Objet Papier* is the first and eponym publication of the label.
    It is both the conclusion and the synthesis of a trilogy which started with
    the video called *[Rejet / Rejection](http://rejet-rejection.tumblr.com/)*
    and *[Et les mouettes…](https://vimeo.com/162227173)* The first video
    creation was about integration in urban areas, adaptation to an unknown
    environment and the difficulty to embrace a specific community. The second
    one was about reappropriation and psychological reconstruction after the
    terrorist attacks in Paris. As the result of an abandoned magazine project,
    *Objet Papier* is a the result of a work that deals with toxic obsessions,
    questions the consistency of printed press and studies previous subjects
    with a different eye."
video:
  has-video: false
  video: ""
  video-color: ""
price:
  amount: 5
  shipping: 1.94
  is-soldout: true
---
