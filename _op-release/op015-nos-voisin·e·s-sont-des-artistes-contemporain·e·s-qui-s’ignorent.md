---
title: OP015 Nos voisin·e·s sont des artistes contemporain·e·s qui s’ignorent.
layout: project
number: 15
draft: false
auteur:
  - name: Sarah Saulnier de Praingy
    website: www.ssdp.fr
name: Nos voisin·e·s sont des artistes contemporain·e·s qui s’ignorent.
date-str: 10/22
black-cover-image: /assets/uploads/op015-0.jpg
white-cover-image: /assets/uploads/op015-cover-white.jpg
images:
  - /assets/uploads/op015-0.jpg
  - /assets/uploads/op015-1.jpg
  - /assets/uploads/op015-2.jpg
  - /assets/uploads/op015-3.jpg
  - /assets/uploads/op015-4.jpg
fr:
  format: 11x16cm
  nb-pages: "32"
  reliure: Piqûre à cheval & colle
  impression: Numérique quadri & noir seul
  exemplaires: "35"
  description: Nos voisin·e·s sont des artistes contemporain·e·s qui s’ignorent
    est le quatrième numéro de la collection.collection éditée par Objet Papier.
    La collection.collection explore les recoins de nos environnements
    quotidiens, en dévoilant les traces retrouvées sur nos extensions de mémoire
    numérique.
en:
  format: 11x16cm
  nb-pages: "32"
  reliure: Saddle stitched & glue
  impression: Quadricolor digital
  exemplaires: "35"
  description: Our neighbors are unaware they are contemporary artists. Fourth
    issue of collection.collection, published by Objet Papier. The
    collection.collection explores our daily environments, revealing lost and
    found parts of our digital memory extensions.
video:
  has-video: false
price:
  is-soldout: true
  amount: 9
  shipping: 2.32
---
