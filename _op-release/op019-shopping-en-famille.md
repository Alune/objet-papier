---
title: OP019 Shopping en famille.
layout: project
number: 19
draft: false
auteur:
  - name: Emily Druzinec
    website: https://www.instagram.com/cosmofall/
name: Shopping en famille.
date-str: 12/2022
black-cover-image: /assets/uploads/op019-1.jpg
white-cover-image: /assets/uploads/op019-cover-white.jpg
images:
  - /assets/uploads/op019-1.jpg
  - /assets/uploads/op019-2.jpg
  - /assets/uploads/op019-3.jpg
  - /assets/uploads/op019-4.jpg
  - /assets/uploads/op019-5.jpg
fr:
  format: 11x16cm
  nb-pages: "32"
  reliure: Piqûre à cheval & colle
  impression: Numérique quadri & noir seul
  exemplaires: "35"
  description: Shopping en famille est le septième numéro de la
    collection.collection éditée par Objet Papier. La collection.collection
    explore les recoins de nos environnements quotidiens, en dévoilant les
    traces retrouvées sur nos extensions de mémoire numérique.
en:
  format: 11x16cm
  nb-pages: "32"
  reliure: Saddle stitched & glue
  impression: Quadricolor digital
  exemplaires: "35"
  description: Family Shopping. Seventh issue of collection.collection, published
    by Objet Papier. The collection.collection explores our daily environments,
    revealing lost and found parts of our digital memory extensions.
video:
  has-video: false
price:
  is-soldout: false
  amount: 9
  shipping: 2.32
---
