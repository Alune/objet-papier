---
title: OP022 Selected Type Works 11-22
layout: project
number: 22
draft: false
auteur:
  - name: Corentin Moussard
    website: https://alune.fr/
name: Selected Type Works 11-22
date-str: 02/2023
black-cover-image: /assets/uploads/op022-1.jpg
white-cover-image: /assets/uploads/op022-cover-white.jpg
images:
  - /assets/uploads/op022-1.jpg
  - /assets/uploads/op022-2.jpg
  - /assets/uploads/op022-3.jpg
  - /assets/uploads/op022-4.jpg
  - /assets/uploads/op022-5.jpg
  - /assets/uploads/op022-6.jpg
fr:
  format: 9 x 13,5 cm
  nb-pages: "48"
  reliure: Spirales Relicoil
  impression: Photocopieur Noir
  exemplaires: "30"
  description: Comme son titre l’indique, Selected Type Workd 11-22 regroupe plus
    d’une cinquantaine de croquis typographiques réalisés par le designer
    Corentin Moussard durant les dix dernières années. Brouillons de travail,
    exercices de style ou simples récréations, ces dessins permettent de percer
    l’intimité obsessionnelle d’un artiste typographe.
en:
  format: 9 x 13,5 cm
  nb-pages: "48"
  reliure: Relicoil Spirals
  impression: Black Copy Machine
  exemplaires: "30"
  description: As its title suggests it, Selected Type Works 11-22 show more than
    fifty typographic sketches drawn by designer Corentin Moussard over the past
    ten years. Work drafts, style exercises or for-fun sketches, these drawings
    allow you to discover the obsessive intimacy of a typographic artist.
video:
  has-video: false
price:
  is-soldout: true
  amount: 6
  shipping: 2.32
---
