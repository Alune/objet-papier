---
title: OP017 J’avais trop la dalle pour faire une meilleure photo de ce plat.
layout: project
number: 17
draft: false
auteur:
  - name: Juliette Bernachot
name: J’avais trop la dalle pour faire une meilleure photo de ce plat.
date-str: 10/2022
black-cover-image: /assets/uploads/op017-1.jpg
white-cover-image: /assets/uploads/op017-cover-white.jpg
images:
  - /assets/uploads/op017-1.jpg
  - /assets/uploads/op017-2.jpg
  - /assets/uploads/op017-3.jpg
  - /assets/uploads/op017-4.jpg
  - /assets/uploads/op017-5.jpg
fr:
  format: 11x16cm
  nb-pages: "36"
  reliure: Piqûre à cheval & colle
  impression: Numérique quadri & noir seul
  exemplaires: "35"
  description: J’avais trop la dalle pour faire une meilleure photo de ce plat est
    le sixième numéro de la collection.collection éditée par Objet Papier. La
    collection.collection explore les recoins de nos environnements quotidiens,
    en dévoilant les traces retrouvées sur nos extensions de mémoire numérique.
en:
  format: 11x16cm
  nb-pages: "36"
  reliure: Saddle stitched & glue
  impression: Quadricolor digital
  exemplaires: "35"
  description: I was starving too much to take a better picture of this meal.
    Sixth issue of collection.collection, published by Objet Papier. The
    collection.collection explores our daily environments, revealing lost and
    found parts of our digital memory extensions.
video:
  has-video: false
price:
  is-soldout: true
  amount: 9
  shipping: 2.32
---
