---
title: OP004 Sinner
layout: project
number: 4
draft: false
auteur:
  - name: Guillaume Rossi
    website: http://guillaumerossi.fr
name: Sinner
date-str: 10/2017
black-cover-image: /assets/uploads/op004-cover-black.jpg
white-cover-image: /assets/uploads/op004-cover-white.jpg
images:
  - /assets/uploads/op004-1.jpg
  - /assets/uploads/op004-2.jpg
  - /assets/uploads/op004-3.jpg
  - /assets/uploads/op004-4.jpg
  - /assets/uploads/op004-5.jpg
  - /assets/uploads/op004-6.jpg
  - /assets/uploads/op004-7.jpg
fr:
  format: 210 x 280 mm
  nb-pages: "48"
  reliure: Piqûre à cheval
  impression: Numérique quadri
  exemplaires: 100 numérotés
  description: "Constitué en diptyque édition et vidéo, *SINNER* est une oeuvre
    post-moderne graphiquement engagée, confrontant les époques et leurs
    enfermements dans une esthétique internet wave, associant architecture,
    collages et poésie. Inspiré librement par le morceau de Max Richter :
    “Garden (1973) / Interior” paru en 2002 sur Memoryhouse, Guillaume Rossi
    livre ici une relecture dystopique d’un passage clé de la Genèse, privant
    Adam de son Ève, le transportant dans une prison moderne aux barreaux
    fluctuants."
en:
  format: 210 x 280 mm
  nb-pages: "48"
  reliure: Saddle stitched
  impression: Quadricolor digital
  exemplaires: 100 hand numbered
  description: "*SINNER* is a video and edition diptych creation. With a
    post-modern and internet wave approach, this involved work deals with
    differents eras and their confinements. It associates architecture, collages
    and poetry. Freely inspired by Max Richter’s “Garden (1973) / Interior”,
    Guillaume Rossi offers a dystopian rereading of an important section of the
    Genesis with a lonely Adam in a modern prison with fluctuating bars."
video:
  has-video: true
  video: "235078540"
  video-color: c40018
price:
  amount: 7
  shipping: 4
  is-soldout: false
---
