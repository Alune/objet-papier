---
title: OP005 Skit
layout: project
number: 5
draft: false
auteur:
  - name: Ronan Deshaies
    website: http://cargocollective.com/ronandeshaies
name: Skit
date-str: 02/2018
black-cover-image: /assets/uploads/op005-cover-black.jpg
white-cover-image: /assets/uploads/op005-cover-white.jpg
images:
  - /assets/uploads/op005-1.jpg
  - /assets/uploads/op005-2.jpg
  - /assets/uploads/op005-3.jpg
  - /assets/uploads/op005-4.jpg
  - /assets/uploads/op005-5.jpg
  - /assets/uploads/op005-6.jpg
fr:
  format: A5
  nb-pages: "32"
  reliure: Piqûre à cheval
  impression: Risographie bichromie
  exemplaires: 60 numérotés
  description: >-
    Résultat de deux ans de travail à travers le continent Européen, *SKIT*,
    interlude cathartique produit dans l’urgence de l’expression, livre un
    hommage critique à l’Union perdue. Reflet du mouvement possible, *SKIT*
    puise dans toutes les formes de la culture DIY pour livrer le portrait d’une
    génération tiraillée entre la célébration et l’abandon politique. Produit
    analogiquement par Ronan Deshaies puis retraité numériquement par Morgane
    Bartoli, l’édition papier s’accompagne d’une création vidéo orchestrée par
    Corentin Moussard.


    [EXPERIENCE TOTALE](https://objetpapier.fr/skit/)
en:
  format: A5
  nb-pages: "32"
  reliure: Saddle stitched
  impression: Risograph 2 colors
  exemplaires: 60 hand numbered
  description: >-
    *SKIT* is the result of a two years work across the European continent.
    Produced as urgently as a scream, it’s a cathartic skit that pays a critical
    tribute to the lost Union. This work is a reflection about freedom of
    movement inspired by all the DIY culture. It talks about a generation that
    doesn’t want to choose between the celebration and the abandonment of
    politics. This is an analog work produced by Ronan Deshaies, then digitally
    reprocessed by Morgane Bartoli. The paperprinted version is accompanied by a
    video creation synchronized by Corentin Moussard.


    [TOTAL EXPERIENCE](https://objetpapier.fr/skit/)
video:
  has-video: false
  video: ""
  video-color: ""
price:
  amount: 9
  shipping: 1.94
  is-soldout: true
---
