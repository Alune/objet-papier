---
title: OP010 Souvenirs photographiques du monde réel
layout: project
number: 10
draft: false
auteur:
  - name: Corentin Moussard
    website: https://alune.fr/
  - name: Morgane Bartoli
    website: https://morganebartoli.com/
  - name: Juliette Bernachot
  - name: Ronan Deshaies
    website: https://cargocollective.com/ronandeshaies
name: Souvenirs photographiques du monde réel
date-str: 06/2021
black-cover-image: /assets/uploads/op010-black.jpg
white-cover-image: /assets/uploads/op010-white.jpg
images:
  - /assets/uploads/op010-0.jpg
  - /assets/uploads/op010-black.jpg
  - /assets/uploads/op010-1.jpg
  - /assets/uploads/op010-2.jpg
  - /assets/uploads/op010-3.jpg
  - /assets/uploads/op010-4.jpg
  - /assets/uploads/op010-5.jpg
  - /assets/uploads/op010-6.jpg
fr:
  format: A4
  nb-pages: 24 X3
  reliure: Piqûre à cheval
  impression: Photocopieur quadri
  exemplaires: "30"
  description: >-
    «Souvenirs photographiques du monde réel» est un triptyque de fanzines
    rassemblant les images enfermées, informatisées et libérées provenant
    des téléphones de près de 50 contributeurs. Classées par thèmes puis
    altérées selon les consignes d’un programme procédural mis au point pour
    l’occasion, elles ont été rassemblées en 30 tableaux témoignant d’une
    époque confinée et informatisée à l’extrême.


    Réalisé dans le cadre de l’unité de publications improvisée « My Favorite Things » de Laura Morsch Kihn lors de la 11ème édition du FILAF à Perpignan.


    [Rencontrez Jeanne-Claude Numérique ici](https://filaf.objetpapier.fr/).
en:
  format: A4
  nb-pages: 24 X3
  reliure: Saddle stitched
  impression: Quadricolor Copy Machine
  exemplaires: "30"
  description: >-
    “Souvenirs photographiques du monde réel” is a triptych of fanzines
    bringing together locked, computerized and unlocked images from the phones
    of nearly 50 contributors. Classified by theme and altered according to the
    instructions of a procedural program, it has been brought together in 30
    boards testifying about a confined and extremely computerized era.


    Produced with the Laura Morsch Kihn's improvised publication unit "My Favorite Things" during the FILAF #11 in Perpignan.


    [Meet Jeanne-Claude Numérique here.](https://filaf.objetpapier.fr/)
video:
  has-video: false
price:
  is-soldout: true
  amount: 0
  shipping: 0
---
