---
title: OP006 Bali, feuilles rapportées
layout: project
number: 6
draft: false
auteur:
  - name: Mamzelle Heing
    website: https://heingshop.com/
    Key: null
name: Bali, feuilles rapportées
date-str: 09/2018
black-cover-image: /assets/uploads/op006-cover-black.jpg
white-cover-image: /assets/uploads/op006-cover-white.jpg
images:
  - /assets/uploads/op006-1.jpg
  - /assets/uploads/op006-2.jpg
  - /assets/uploads/op006-3.jpg
  - /assets/uploads/op006-4-2132df.jpg
  - /assets/uploads/op006-5.jpg
fr:
  format: A5
  nb-pages: "36"
  reliure: Japonaise
  impression: Numérique quadri
  exemplaires: "70"
  description: Imaginé comme le croisement entre un herbier et un carnet de
    voyage, *Bali, feuilles rapportées*, édition organique et artisanale de
    l’illustratrice Mamzelle Heing est le récit intime d’un voyage
    reconstructeur à l’origine d’une renaissance. En confrontant ses souvenirs
    photographiques à une flore imaginaire, l’artiste remonte à la source de la
    créativité humaine et livre son interprétation du dialogue entre culture et
    nature.
en:
  format: A5
  nb-pages: "36"
  reliure: Japanese
  impression: Quadricolor digital
  exemplaires: "70"
  description: Imagined as the crossroads of a herbarium and a travel diary,
    *Bali, feuilles rapportées*, ("Bali, brought leafs") an organic and craft
    edition created by the illustrator Mamzelle Heing is the intimate story of a
    reconstructive journey which makes a rebirth possible. By confronting her
    photographic memories with an imaginary flora, the artist goes back to the
    roots of human creativity and delivers her interpretation of the dialogue
    between culture and nature.
video:
  has-video: false
  video: ""
  video-color: ""
price:
  amount: 12
  shipping: 1.94
  is-soldout: true
---
