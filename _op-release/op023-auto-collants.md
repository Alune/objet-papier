---
title: OP023 Auto+Collants.
layout: project
number: 23
draft: false
auteur:
  - name: Paul Marique
    website: https://paulmarique.be/
name: Auto+Collants.
date-str: 04/2023
black-cover-image: /assets/uploads/op023-1.jpg
white-cover-image: /assets/uploads/op023-cover-white-bidouille.jpg
images:
  - /assets/uploads/op023-1.jpg
  - /assets/uploads/op023-2.jpg
  - /assets/uploads/op023-3.jpg
  - /assets/uploads/op023-4.jpg
  - /assets/uploads/op023-5.jpg
  - /assets/uploads/op023-6.jpg
fr:
  format: 11x16cm
  nb-pages: "36"
  reliure: Piqûre à cheval & colle
  impression: Numérique quadri & noir seul
  exemplaires: "50"
  description: Auto+Collants est le dixième numéro de la collection.collection
    éditée par Objet Papier. La collection.collection explore les recoins de nos
    environnements quotidiens, en dévoilant les traces retrouvées sur nos
    extensions de mémoire numérique.
en:
  format: 11x16cm
  nb-pages: "36"
  reliure: Saddle stitched & glue
  impression: Quadricolor digital
  exemplaires: "50"
  description: >-
    Car Stickers.\

    Tenth issue of collection.collection, published by Objet Papier. The collection.collection explores our daily environments, revealing lost and found parts of our digital memory extensions.
video:
  has-video: false
price:
  is-soldout: false
  amount: 9
  shipping: 2.32
---
