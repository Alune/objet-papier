---
title: OP020 C’est l’époque de l’année où les sapins meurent sur le trottoir.
layout: project
number: 20
draft: false
auteur:
  - name: Ronan Deshaies
    website: https://cargocollective.com/ronandeshaies
name: C’est l’époque de l’année où les sapins meurent sur le trottoir.
date-str: 12/2022
black-cover-image: /assets/uploads/op020-1.jpg
white-cover-image: /assets/uploads/op020-cover-white.jpg
images:
  - /assets/uploads/op020-1.jpg
  - /assets/uploads/op020-2.jpg
  - /assets/uploads/op020-3.jpg
  - /assets/uploads/op020-4.jpg
  - /assets/uploads/op020-5.jpg
fr:
  format: 11x16cm
  nb-pages: "24"
  reliure: Piqûre à cheval & colle
  impression: Numérique quadri & noir seul
  exemplaires: "35"
  description: C’est l’époque de l’année où les sapins meurent sur le trottoir est
    le huitième numéro de la collection.collection éditée par Objet Papier. La
    collection.collection explore les recoins de nos environnements quotidiens,
    en dévoilant les traces retrouvées sur nos extensions de mémoire numérique.
en:
  format: 11x16cm
  nb-pages: "24"
  reliure: Saddle stitched & glue
  impression: Quadricolor digital
  exemplaires: "35"
  description: It’s that time of the year when pine trees are dying on the
    sidewalk. Eighth issue of collection.collection, published by Objet Papier.
    The collection.collection explores our daily environments, revealing lost
    and found parts of our digital memory extensions.
video:
  has-video: false
price:
  is-soldout: true
  amount: 9
  shipping: 2.32
---
