---
title: OP027 fair dogs
layout: project
number: 27
draft: false
auteur:
  - name: Objet Papier
name: fair dogs
date-str: 09/2023
black-cover-image: /assets/uploads/op027-cover-black.jpg
white-cover-image: /assets/uploads/op027-cover-white.jpeg
images:
  - /assets/uploads/op027-cover-black.jpg
  - /assets/uploads/op027-01.jpg
  - /assets/uploads/op027-02.jpg
  - /assets/uploads/op027-03.jpg
  - /assets/uploads/op027-04.jpg
  - /assets/uploads/op027-05.jpg
fr:
  format: A6
  nb-pages: "40"
  reliure: Piqûre
  impression: Riso quadri
  exemplaires: "50"
  description: Lors de l’édition 2022 du salon Libros Mutantes, à Madrid, les
    chiens étaient interdits d’entrée. L’année suivante, après avoir reçu de
    multiples plaintes, les organisateurs ont permis à nos amis canins de
    profiter, eux aussi, d’un week-end dédié à la micro-édition. Ce zine leur
    rend hommage.
en:
  impression: Risograph quadricolor
  format: A6
  nb-pages: "40"
  reliure: Saddle stitched
  exemplaires: "50"
  description: For the 2022 edition of Madrid’s Libros Mutantes fair, dogs were
    not allowed in. The following year, after receiving several complaints, the
    organizers revised their judgment and decided to offer to our canine friends
    a weekend dedicated to micro-publishing. Threw this zine, we are paying a
    tribute to them.
video:
  has-video: false
price:
  is-soldout: true
  amount: 3
  shipping: 2.32
---
