---
title: OP018 Complexe(s)
layout: project
number: 18
draft: false
auteur:
  - name: Juliette Bernachot
name: Complexe(s)
date-str: 11/2022
black-cover-image: /assets/uploads/op018-1.jpg
white-cover-image: /assets/uploads/op018-cover-white.jpg
images:
  - /assets/uploads/op018-1.jpg
  - /assets/uploads/op018-2.jpg
  - /assets/uploads/op018-3.jpg
  - /assets/uploads/op018-4.jpg
  - /assets/uploads/op018-5.jpg
fr:
  format: 13,5 x 19,5 cm
  nb-pages: "24"
  reliure: Piqûre à cheval
  impression: Risographie noir & doré métallique
  exemplaires: "40"
  description: Avec Complexe(s), Juliette Bernachot s'empare de sept témoignages
    textuels et visuels pour en livrer une relecture aussi graphique qu'intime.
    Elle évoque ainsi le rapport au corps, ses complexes subis et imposés ou la
    nécessité de se les réapproprier. Une façon tangible, pour elle, d'appliquer
    les principes de sororité et de résilience à une œuvre.
en:
  format: 13,5 x 19,5 cm
  nb-pages: "24"
  reliure: Saddle stitched
  impression: Risograph black & metal gold
  exemplaires: "40"
  description: In Complexe(s), Juliette Bernachot reinterprets seven textual and
    visual testimonies with her graphic and intimate style. She thus evokes the
    relationship to the body, its suffered and imposed complexes or the need to
    reclaim them. A real way to apply  principles of sisterhood and resilience
    in a work of art.
video:
  has-video: false
price:
  is-soldout: false
  amount: 9
  shipping: 2.32
---
