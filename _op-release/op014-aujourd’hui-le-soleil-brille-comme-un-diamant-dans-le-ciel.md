---
title: OP014 Aujourd’hui, le soleil brille comme un diamant dans le ciel.
layout: project
number: 14
draft: false
auteur:
  - name: Morgane Bartoli
    website: https://morganebartoli.com/
name: Aujourd’hui, le soleil brille comme un diamant dans le ciel.
date-str: 05/2022
black-cover-image: /assets/uploads/op014-black2.jpg
white-cover-image: /assets/uploads/op014-white2.jpg
images:
  - /assets/uploads/op014-black2.jpg
  - /assets/uploads/op014-01.jpg
  - /assets/uploads/op014-02.jpg
  - /assets/uploads/op014-03.jpg
  - /assets/uploads/op014-04.jpg
fr:
  format: 11x16cm
  nb-pages: "28"
  reliure: Piqûre à cheval & colle
  impression: Numérique quadri & noir seul
  exemplaires: "35"
  description: Aujourd’hui, le soleil brille comme un diamant dans le ciel est le
    troisième numéro de la collection.collection éditée par Objet Papier. La
    collection.collection explore les recoins de nos environnements quotidiens,
    en dévoilant les traces retrouvées sur nos extensions de mémoire numérique.
en:
  format: 11x16cm
  nb-pages: "28"
  reliure: Saddle stitched & glue
  impression: Quadricolor digital
  exemplaires: "35"
  description: Today, the sun shines bright like a diamond in the sky. Third issue
    of collection.collection, published by Objet Papier. The
    collection.collection explores our daily environments, revealing lost and
    found parts of our digital memory extensions.
video:
  has-video: false
price:
  is-soldout: true
  amount: 9
  shipping: 2.32
---
