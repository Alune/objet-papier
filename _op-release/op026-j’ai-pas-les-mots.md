---
title: OP026 J’ai pas les mots
layout: project
number: 26
draft: false
auteur:
  - name: Ben Zedrine
name: J’ai pas les mots
date-str: 05/2023
black-cover-image: /assets/uploads/sr5-6_sud_g23060115420-copie.jpg
white-cover-image: /assets/uploads/sr5-6_sud_g23060115380-copie.jpg
images:
  - /assets/uploads/sr5-6_sud_g23060115420-copie.jpg
  - /assets/uploads/sr5-6_sud_g23060115430-copie.jpg
  - /assets/uploads/sr5-6_sud_g23060115431-copie.jpg
  - /assets/uploads/sr5-6_sud_g23060115432-copie.jpg
  - /assets/uploads/sr5-6_sud_g23060115433-copie.jpg
  - /assets/uploads/sr5-6_sud_g23060115440-copie.jpg
fr:
  format: A5
  nb-pages: "28"
  reliure: Piqûre à cheval
  impression: Photocopieur
  exemplaires: 49,3
  description: À partir du 20 janvier 2023, la presse française s’est faite l’écho
    des manifestations contre la réforme des retraites. À la une, les opposants
    formulent leur proposition, synthétisent leur pensée sur des pancartes
    faites main. Des mots, et parfois des chiffres, niés, détournés et moqués
    par des politiciens experts en novlangue. Face à cette impasse, Ben Zedrine
    propose un adieu au langage. Un défilé silencieux face à la surdité
    gouvernementale.
en:
  format: A5
  nb-pages: "28"
  reliure: Saddle stitch
  impression: Copy Machine
  exemplaires: "49"
  description: From January 20, 2023, the French press talked about the
    demonstrations against the pension reform. On the front page, the opponents
    formulate their proposal, synthesize their ideas with handmade placards.
    Words (or numbers), denied, diverted and mocked by a government specialized
    in Newspeak. In response to this impasse, Ben Zedrine offers a farewell to
    language. A silent parade against deaf politicians.
video:
  has-video: false
price:
  is-soldout: false
  amount: 5
  shipping: 0
---
