---
title: OP013 Messages sur les murs.
layout: project
number: 13
draft: false
auteur:
  - name: Ben Zedrine
name: Messages sur les murs.
date-str: 05/2022
black-cover-image: /assets/uploads/op013-black.jpg
white-cover-image: /assets/uploads/op013-white.jpg
images:
  - /assets/uploads/op013-black.jpg
  - /assets/uploads/op013-01.jpg
  - /assets/uploads/op013-02.jpg
  - /assets/uploads/op013-03.jpg
  - /assets/uploads/op013-04.jpg
  - /assets/uploads/op013-05.jpg
fr:
  format: 11x16cm
  nb-pages: "36"
  reliure: Piqûre à cheval & colle
  impression: Numérique quadri & noir seul
  exemplaires: "35"
  description: Messages sur les murs est le deuxième numéro de la
    collection.collection éditée par Objet Papier. La collection.collection
    explore les recoins de nos environnements quotidiens, en dévoilant les
    traces retrouvées sur nos extensions de mémoire numérique.
en:
  format: 11x16cm
  nb-pages: "36"
  reliure: Saddle stitched & glue
  impression: Quadricolor digital
  exemplaires: "35"
  description: Messages on walls. Second issue of collection.collection, published
    by Objet Papier. The collection.collection explores our daily environments,
    revealing lost and found parts of our digital memory extensions.
video:
  has-video: false
price:
  is-soldout: true
  amount: 9
  shipping: 2.32
---
