---
title: OP025 Oups, nous avons rencontré une erreur, veuillez réessayer ultérieurement.
layout: project
number: 25
draft: false
auteur:
  - name: Joséphine Brueder
    website: https://josephinebrueder.com/
name: Oups, nous avons rencontré une erreur, veuillez réessayer ultérieurement.
date-str: 04/2023
black-cover-image: /assets/uploads/op025-1.jpg
white-cover-image: /assets/uploads/op025-cover-white-bidouille.jpg
images:
  - /assets/uploads/op025-1.jpg
  - /assets/uploads/op025-2.jpg
  - /assets/uploads/op025-3.jpg
  - /assets/uploads/op025-5.jpg
  - /assets/uploads/op025-4.jpg
  - /assets/uploads/op025-6.jpg
fr:
  format: 11x16cm
  nb-pages: "32"
  reliure: Piqûre à cheval & colle
  impression: Numérique quadri & noir seul
  exemplaires: "50"
  description: Oups, nous avons rencontré une erreur, veuillez réessayer
    ultérieurement est le onzième numéro de la collection.collection éditée par
    Objet Papier. La collection.collection explore les recoins de nos
    environnements quotidiens, en dévoilant les traces retrouvées sur nos
    extensions de mémoire numérique.
en:
  format: 11x16cm
  nb-pages: "32"
  reliure: Saddle stitched & glue
  impression: Quadricolor digital
  exemplaires: "50"
  description: >-
    Oops, an error has occurred, please try again later.\

    Twelfth issue of collection.collection, published by Objet Papier. The collection.collection explores our daily environments, revealing lost and found parts of our digital memory extensions.
video:
  has-video: false
price:
  is-soldout: false
  amount: 9
  shipping: 2.32
---
