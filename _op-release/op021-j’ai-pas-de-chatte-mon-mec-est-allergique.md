---
title: OP021 J’ai pas de chatte, mon mec est allergique.
layout: project
number: 21
draft: false
auteur:
  - name: Juliette Bernachot
name: J’ai pas de chatte, mon mec est allergique.
date-str: 12/2022
black-cover-image: /assets/uploads/op021-1.jpg
white-cover-image: /assets/uploads/op021-cover-white.jpg
images:
  - /assets/uploads/op021-1.jpg
  - /assets/uploads/op021-2.jpg
  - /assets/uploads/op021-3.jpg
  - /assets/uploads/op021-4.jpg
  - /assets/uploads/op021-5.jpg
  - /assets/uploads/op021-6.jpg
fr:
  format: 11x16cm
  nb-pages: "32"
  reliure: Piqûre à cheval & colle
  impression: Numérique quadri & noir seul
  exemplaires: "35"
  description: J’ai pas de chatte, mon mec est allergique est le neuvième numéro
    de la collection.collection éditée par Objet Papier. La
    collection.collection explore les recoins de nos environnements quotidiens,
    en dévoilant les traces retrouvées sur nos extensions de mémoire numérique.
en:
  format: 11x16cm
  nb-pages: "32"
  reliure: Saddle stitched & glue
  impression: Quadricolor digital
  exemplaires: "35"
  description: I don’t have a cat, my boyfriend is allergic. Ninth issue of
    collection.collection, published by Objet Papier. The collection.collection
    explores our daily environments, revealing lost and found parts of our
    digital memory extensions.
video:
  has-video: false
price:
  is-soldout: true
  amount: 9
  shipping: 2.32
---
