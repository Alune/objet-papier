---
title: opexp016
layout: experiment
number: 16
auteur:
  - name: "Morgane Bartoli "
    website: https://morganebartoli.com/
  - name: "Corentin Moussard "
    website: https://alune.fr/
name: "Teaser OP007 #4"
date-str: 03/2018
thumbnail: /assets/uploads/opexp016-white.jpg
thumbnail-hover: /assets/uploads/opexp016-black.jpg
video:
  has-video: true
  video: "433791290"
exp-meta-fr:
  - key: Format
    value: Vidéo
  - key: Durée
    value: 0:13’
  - key: Nombre
    value: "3"
  - key: Taille
    value: 1080 x 1920 px
exp-meta-en:
  - key: Format
    value: Video
  - key: "Duration "
    value: 0:13’
  - key: Amount
    value: "3"
  - key: Size
    value: 1080 x 1920 px
---
