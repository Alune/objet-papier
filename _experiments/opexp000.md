---
title: opexp000
layout: experiment
number: 0
auteur:
  - name: "Morgane Bartoli "
    website: https://morganebartoli.com/
  - name: "Corentin Moussard "
    website: https://alune.fr/
name: Site web Objet Papier
name-en: Objet Papier website
date-str: 10/2017
thumbnail: /assets/uploads/opexp000-white0.jpg
thumbnail-hover: /assets/uploads/opexp000-black0.jpg
video:
  has-video: true
  video: "437188957"
  video-color: "#FFFFFF"
exp-meta-fr:
  - key: Format
    value: Site web
  - key: Lien
    value: "[objetpapier.fr](https://objetpapier.fr/)"
  - key: Taille
    value: Responsive
exp-meta-en:
  - key: Format
    value: Website
  - value: "[objetpapier.fr](https://objetpapier.fr/)"
    key: Link
  - key: Size
    value: Responsive
---
