---
title: opexp019
layout: experiment
number: 19
auteur:
  - name: "Morgane Bartoli "
    website: https://morganebartoli.com/
  - name: "Corentin Moussard "
    website: https://alune.fr/
name: Flyer Print-it
name-en: Print-it flyer
date-str: 03/2018
thumbnail: /assets/uploads/opexp019-white.jpg
thumbnail-hover: /assets/uploads/opexp019.jpg
images:
  - /assets/uploads/opexp019.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Flyer
  - key: Impression
    value: Numérique noir
  - key: Exemplaires
    value: "100"
  - key: Papier
    value: Chromolux Alu Silver
  - key: "Grammage "
    value: "90"
  - key: Taille
    value: A6
exp-meta-en:
  - value: Flyer
    key: Format
  - key: Print technique
    value: Black digital print
  - key: Copies
    value: "100"
  - key: Paper
    value: Chromolux Alu Silver
  - key: Grammage
    value: "90"
  - key: Size
    value: A6
---
