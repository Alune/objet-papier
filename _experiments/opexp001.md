---
title: opexp001
layout: experiment
number: 1
auteur:
  - name: "Morgane Bartoli "
    website: https://morganebartoli.com/
name: Teaser OP003
name-en: OP003 teaser
date-str: 04/2017
thumbnail: /assets/uploads/opexp001_white.jpg
thumbnail-hover: /assets/uploads/opexp001_black.jpg
images: []
video:
  has-video: true
  video: "433744293"
exp-meta-fr:
  - key: Format
    value: Video
  - value: 0:12’
    key: "Durée "
  - key: Taille
    value: 800 x 600 px
exp-meta-en:
  - value: Vidéo
    key: Format
  - value: "0:12’ "
    key: Duration
  - key: Size
    value: 800 x 600 px
---
