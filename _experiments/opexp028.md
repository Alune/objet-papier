---
title: opexp028
layout: experiment
number: 28
auteur:
  - name: Morgane Bartoli
    website: https://morganebartoli.com/
  - name: Ronan Deshaies
    website: https://cargocollective.com/ronandeshaies
  - name: "Corentin Moussard "
    website: https://alune.fr/
name: Conférence Print-it
name-en: "Print-it talk "
date-str: 03/2019
thumbnail: /assets/uploads/opexp028-2b.jpg
images:
  - /assets/uploads/opexp028-1b.jpg
  - /assets/uploads/opexp028-3b.jpg
  - /assets/uploads/opexp028-2b.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Conférence
  - key: "Sujet "
    value: |-
      CSS print \
      Relation imprimé/numérique\
      OP007 : Print-it
  - key: "Durée "
    value: 30/40 min
  - key: Lieux
    value: |-
      Charleroi (BL), Paris (FR)\
      Poitiers (FR), Gand (BL)\
      Roubaix (FR)
exp-meta-en:
  - key: Format
    value: Talk
  - key: "Subject "
    value: |-
      CSS Print  \
      Relationship between print and digital\
      OP007 : Print-it
  - key: Duration
    value: 30/40 min
  - key: Locations
    value: |-
      Charleroi (BL), Paris (FR)\
      Poitiers (FR), Gand (BL)\
      Roubaix (FR)
---
