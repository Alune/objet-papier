---
title: opexp017
layout: experiment
number: 17
auteur:
  - name: Morgane Bartoli
    website: https://morganebartoli.com/
  - name: Juliette Bernachot
  - name: Corentin Moussard
    website: https://alune.fr/
  - name: Ronan Deshaies
    website: https://cargocollective.com/ronandeshaies
name: Print-it, à la rencontre de ses lecteurs !
name-en: Print-it, meeting the readers !
date-str: 15/03/2019 — 10/12/2019
thumbnail: /assets/uploads/opexp017-0.jpg
thumbnail-hover: ""
images:
  - /assets/uploads/opexp017-7b.jpg
  - /assets/uploads/opexp017-6b.jpg
  - /assets/uploads/opexp017-5.jpg
  - /assets/uploads/opexp017-4.jpg
  - /assets/uploads/opexp017-3.jpg
  - /assets/uploads/opexp017-2.jpg
  - /assets/uploads/opexp017-1.jpg
  - /assets/uploads/opexp017-0.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: |-
      Tournée de présentation\
      Conférence + stand d’impression
  - key: "Durée "
    value: 19 étapes, 1 an
  - key: Lieux
    value: 10 villes, 2 pays
exp-meta-en:
  - key: Format
    value: |-
      Presentation tour\
      Conference + printing stand
  - key: "Duration "
    value: 19 steps, 1 year
  - key: Location
    value: 10 cities, 2 countries
---
