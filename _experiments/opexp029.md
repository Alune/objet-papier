---
title: opexp029
layout: experiment
number: 29
auteur:
  - name: Morgane Bartoli
    website: https://morganebartoli.com/
name: "Carte de visite #2"
name-en: "Business card #2"
date-str: 03/2019
thumbnail: /assets/uploads/opexp029-white.jpg
thumbnail-hover: /assets/uploads/opexp029.jpg
images:
  - /assets/uploads/opexp029.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Carte de visite
  - key: Impression
    value: Numérique noir
  - key: Exemplaires
    value: "200"
  - key: Taille
    value: 8,5 x 5,5 cm
exp-meta-en:
  - key: Format
    value: Business card
  - key: Print technique
    value: Black digital print
  - key: Copies
    value: "200"
  - key: Size
    value: 8.5 x 5.5 cm
---
