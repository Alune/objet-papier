---
title: opexp011
layout: experiment
number: 11
auteur:
  - name: "Morgane Bartoli "
    website: https://morganebartoli.com/
name: Objet Papier x Riso Friends
date-str: 05/2018
thumbnail: /assets/uploads/opexp011-white.jpg
thumbnail-hover: /assets/uploads/opexp011-1.jpg
images:
  - /assets/uploads/opexp011-7.jpg
  - /assets/uploads/opexp011-8.jpg
  - /assets/uploads/opexp011-6.jpg
  - /assets/uploads/opexp011-5.jpg
  - /assets/uploads/opexp011-3.jpg
  - /assets/uploads/opexp011-2.jpg
  - /assets/uploads/opexp011-1.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Affiches inspirées du catalogue Objet Papier
  - key: Impression
    value: Risographie bichromie
  - key: Imprimeur
    value: Maison Riso
  - key: Exemplaires
    value: 40 x 7 versions
  - key: Distribution
    value: "[Riso Friends](http://risofriends.com/)"
  - key: Taille
    value: A4
exp-meta-en:
  - key: Format
    value: Posters inspired by the Objet Papier catalog
  - key: Print technique
    value: Risography 2 colors
  - key: "Printer "
    value: Maison Riso
  - key: Copies
    value: 40 x 7 versions
  - key: Distribution
    value: "[Riso Friends](http://risofriends.com/)"
  - key: Size
    value: A4
---
