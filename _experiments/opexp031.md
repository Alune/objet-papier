---
title: opexp031
layout: experiment
number: 31
auteur:
  - name: "Corentin Moussard "
    website: https://alune.fr/
name: Total Experience Skit
name-en: "Skit Total Experience "
date-str: 02/2018
thumbnail: /assets/uploads/opexp031-white.jpg
thumbnail-hover: /assets/uploads/opexp031-black.jpg
video:
  has-video: true
  video: "434156715"
exp-meta-fr:
  - key: Format
    value: Site web
  - key: Lien
    value: "[objetpapier.fr/skit](https://objetpapier.fr/skit/)"
  - key: Taille
    value: Responsive
exp-meta-en:
  - key: Format
    value: Website
  - key: Link
    value: "[objetpapier.fr/skit](https://objetpapier.fr/skit/)"
  - key: Size
    value: Responsive
---
