---
title: opexp024
layout: experiment
number: 24
auteur:
  - name: "Corentin Moussard "
    website: https://alune.fr/
name: "Spécimen web Snap-it "
name-en: Snap-it web specimen
date-str: 05/2019
thumbnail: /assets/uploads/opexp024-white.jpg
thumbnail-hover: /assets/uploads/opexp024-black.jpg
video:
  has-video: true
  video: "433791785"
  video-color: FFFFFF
exp-meta-fr:
  - key: Format
    value: "Site web "
  - key: Lien
    value: "[objetpapier.fr/snap-it](https://objetpapier.fr/snap-it/)"
  - key: Taille
    value: Responsive
exp-meta-en:
  - key: Format
    value: "Website "
  - key: Link
    value: "[objetpapier.fr/snap-it](https://objetpapier.fr/snap-it/)"
  - key: Size
    value: Responsive
---
