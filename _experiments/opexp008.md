---
title: opexp008
layout: experiment
number: 8
auteur:
  - name: Ronan Deshaies
    website: https://cargocollective.com/ronandeshaies
name: Célébrons 68
date-str: 05/2018
thumbnail: /assets/uploads/opexp008-white.jpg
thumbnail-hover: /assets/uploads/opexp008-1.jpg
images:
  - /assets/uploads/opexp008-2.jpg
  - /assets/uploads/opexp008-3.jpg
  - /assets/uploads/opexp008-1.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Affiche
  - key: Impression
    value: "Risographie bichromie "
  - value: Maison Riso
    key: Imprimeur
  - key: Exemplaires
    value: "25"
  - key: Taille
    value: A3
exp-meta-en:
  - key: Format
    value: Poster
  - key: Print technique
    value: "Risography 2 colors "
  - key: Imprimeur
    value: "Maison Riso "
  - key: Copies
    value: "25"
  - key: Size
    value: A3
---
