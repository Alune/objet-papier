---
title: opexp022
layout: experiment
number: 22
auteur:
  - name: "Morgane Bartoli "
    website: https://morganebartoli.com/
  - name: "Corentin Moussard "
    website: https://alune.fr/
  - name: Ronan Deshaies
    website: https://cargocollective.com/ronandeshaies
name: Catalogue d’exposition Computer Grrrls
name-en: Computer Grrrls exhibition catalogue
date-str: 04/2019
thumbnail: /assets/uploads/opexp022-white.jpg
thumbnail-hover: /assets/uploads/opexp022.jpg
images:
  - /assets/uploads/opexp022.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Catalogue
  - key: Impression
    value: Au choix
  - key: Exemplaires
    value: Sur demande
  - key: Distribution
    value: Lors de la Grrrls Tech Zine Fair
  - key: Lien
    value: "[print-it.objetpapier.fr/grrrls-exposition](https://print-it.objetpapie\
      r.fr/grrrls-exposition/)"
  - key: Taille
    value: A5
exp-meta-en:
  - key: Format
    value: Catalog
  - key: Print technique
    value: User's choice
  - key: Copies
    value: On demand
  - key: Distribution
    value: During the Grrrls Tech Zine Fair event
  - key: Link
    value: "[print-it.objetpapier.fr/grrrls-exposition](https://print-it.objetpapie\
      r.fr/grrrls-exposition/)"
  - key: Size
    value: A5
---
