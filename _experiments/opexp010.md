---
title: opexp010
layout: experiment
number: 10
auteur:
  - name: "Morgane Bartoli "
    website: https://morganebartoli.com/
name: "OP003 Sold out "
name-en: ""
date-str: 04/2018
thumbnail: /assets/uploads/opexp010-white.jpg
thumbnail-hover: /assets/uploads/opexp010_black.jpg
video:
  has-video: true
  video: "433745778"
exp-meta-fr:
  - key: Format
    value: Vidéo
  - value: 0:15’
    key: Durée
  - key: Taille
    value: 1080 x 1920 px
exp-meta-en:
  - value: Video
    key: Format
  - key: Duration
    value: 0:15’
  - key: Size
    value: 1080 x 1920 px
---
