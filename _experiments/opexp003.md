---
title: opexp003
layout: experiment
number: 3
auteur:
  - name: Morgane Bartoli
    website: https://morganebartoli.com/
name: "Carte de visite #1"
name-en: "Business card #1"
date-str: 10/2017
thumbnail: /assets/uploads/opexp003-white.jpg
thumbnail-hover: /assets/uploads/opexp003.jpg
images:
  - /assets/uploads/opexp003.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Carte de visite
  - key: Impression
    value: Numérique noir
  - key: Façonnage
    value: Déchirure
  - key: Exemplaires
    value: "50"
  - key: Taille
    value: 8,5 x 5,5 cm
exp-meta-en:
  - key: Format
    value: Business card
  - key: Print technique
    value: Black digital print
  - value: Tearing
    key: Binding
  - key: Copies
    value: "50"
  - key: Size
    value: 8.5 x 5.5 cm
---
