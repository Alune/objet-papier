---
title: opexp009
layout: experiment
number: 9
auteur:
  - name: "Morgane Bartoli "
    website: https://morganebartoli.com/
  - name: "Corentin Moussard "
    website: https://alune.fr/
name: "Photocopies lancement site "
name-en: Website launch copies
date-str: 10/2017
thumbnail: /assets/uploads/mockup-desktop-site.jpg
thumbnail-hover: /assets/uploads/opexp000-1.png
images:
  - /assets/uploads/opexp000-2.png
  - /assets/uploads/op-generic-cover.jpg
  - /assets/uploads/mockup-desktop-site.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Affiches d’ornement d’événements
  - key: Impression
    value: Quadri
  - key: Imprimeur
    value: Photocopieur
  - key: Exemplaires
    value: Non connu
  - key: Taille
    value: A3
exp-meta-en:
  - key: Format
    value: Event ornament posters
  - key: Print technique
    value: Quadri
  - key: "Printer "
    value: Photocopier
  - key: Copies
    value: Unknown
  - key: Size
    value: A3
---
