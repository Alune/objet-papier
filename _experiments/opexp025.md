---
title: opexp025
layout: experiment
number: 25
auteur:
  - name: "Morgane Bartoli "
    website: https://morganebartoli.com/
name: "Spécimen print Snap-it #1"
name-en: "Snap-it print specimen #1"
date-str: 05/2019
thumbnail: /assets/uploads/opexp025-white.jpg
thumbnail-hover: /assets/uploads/opexp025-2.jpg
images:
  - /assets/uploads/opexp025-1.jpg
  - /assets/uploads/opexp025-3.jpg
  - /assets/uploads/opexp025-2.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Affiche
  - key: Impression
    value: Numérique noir
  - key: Exemplaires
    value: "50"
  - key: Papier
    value: Chromolux Alu Silver
  - key: "Grammage "
    value: "90"
  - key: Taille
    value: A3
exp-meta-en:
  - key: Format
    value: Poster
  - key: Print technique
    value: Black digital print
  - key: Copies
    value: "50"
  - value: Chromolux Alu Silver
    key: Paper
  - key: Grammage
    value: "90"
  - key: Size
    value: A3
---
