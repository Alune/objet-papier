---
title: opexp002
layout: experiment
number: 2
auteur:
  - name: Guillaume Rossi
    website: https://guillaumerossi.fr/
name: Flyer OP004
name-en: "OP004 Flyer "
date-str: 08/2017
thumbnail: /assets/uploads/opexp002-white.jpg
thumbnail-hover: /assets/uploads/opexp002.jpg
images:
  - /assets/uploads/opexp002.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Flyer
  - key: Impression
    value: Numérique quadri
  - key: Exemplaires
    value: "100"
  - key: Taille
    value: 21 x 12 cm
exp-meta-en:
  - value: Flyer
    key: Format
  - key: Print technique
    value: Quadricolor digital print
  - key: Copies
    value: "100"
  - key: Size
    value: 21 x 12 cm
---
