---
title: opexp018
layout: experiment
number: 18
auteur:
  - name: Morgane Bartoli
    website: https://morganebartoli.com/
  - name: "Corentin Moussard "
    website: https://alune.fr/
name: Poster Print-it
name-en: "Print-it poster "
date-str: 03/2018
thumbnail: /assets/uploads/opexp018-white.jpg
thumbnail-hover: /assets/uploads/opexp018-1.jpg
images:
  - /assets/uploads/opexp018-2.jpg
  - /assets/uploads/opexp018-1.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Affiche
  - key: Impression
    value: Numérique noir
  - key: Exemplaires
    value: "50"
  - key: Papier
    value: Chromolux Alu Silver
  - key: Grammage
    value: "90"
  - key: Taille
    value: A3
exp-meta-en:
  - key: Format
    value: Poster
  - key: Print technique
    value: Black digital print
  - key: Copies
    value: "50"
  - key: Paper
    value: Chromolux Alu Silver
  - key: Grammage
    value: "90"
  - key: Size
    value: A3
---
