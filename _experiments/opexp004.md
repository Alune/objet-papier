---
title: opexp004
layout: experiment
number: 4
auteur:
  - name: "Morgane Bartoli "
    website: https://morganebartoli.com/
  - name: "Corentin Moussard "
    website: https://alune.fr/
name: Écrans de veille
name-en: Screen savers
date-str: 10/2017
thumbnail: /assets/uploads/opexp004_white.jpg
thumbnail-hover: /assets/uploads/opexp004_black.jpg
video:
  has-video: true
  video: "435551474"
exp-meta-fr:
  - key: Format
    value: Vidéo
  - key: "Durée "
    value: Irrégulière
  - value: "22 "
    key: Nombre
  - key: Taille
    value: 16/9
exp-meta-en:
  - key: Format
    value: Video
  - key: Duration
    value: Irregular
  - key: Number
    value: "22"
  - key: Size
    value: 16/9
---
