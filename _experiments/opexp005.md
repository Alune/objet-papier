---
title: opexp005
layout: experiment
number: 5
auteur:
  - name: Morgane Bartoli
    website: https://morganebartoli.com/
  - name: Juliette Bernachot
  - name: Ronan Deshaies
    website: https://cargocollective.com/ronandeshaies
  - name: Corentin Moussard
    website: https://alune.fr/
  - name: Guillaume Rossi
    website: https://guillaumerossi.fr/
  - name: Sixtyne Perez
name: "Lancement OP004 "
name-en: OP004 launch party
date-str: 19/10/2017
thumbnail: /assets/uploads/opexp005_1.jpg
images:
  - /assets/uploads/opexp005_3.jpg
  - /assets/uploads/opexp005_2.jpg
  - /assets/uploads/opexp005_0.jpg
  - /assets/uploads/opexp005_1.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Projection, DJ sets, vente, rencontre
  - key: Lieu
    value: "Chair de Poule, Paris 11 "
  - key: Photos
    value: Julien Al
exp-meta-en:
  - key: Format
    value: Projection, DJ sets, sales, meeting
  - key: Location
    value: Chair de Poule, Paris 11
  - key: Photos
    value: Julien Al
---
