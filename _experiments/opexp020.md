---
title: opexp020
layout: experiment
number: 20
auteur:
  - name: Morgane Bartoli
    website: https://morganebartoli.com/
  - name: "Manon Gavalda "
name: Présentoirs Print-it
name-en: Print-it display
date-str: 03/2019
thumbnail: /assets/uploads/opexp020-white.jpg
thumbnail-hover: /assets/uploads/opexp020-0.jpg
images:
  - /assets/uploads/opexp020-1.jpg
  - /assets/uploads/opexp020-0.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Présentoirs
  - key: Matériaux
    value: Bois, acier, plastique
  - key: Exemplaires
    value: "2"
  - key: Impression (lettrage)
    value: 3D
  - key: Taille
    value: 30 x 90 cm
exp-meta-en:
  - key: Format
    value: Display
  - value: Wood, steel, plastic
    key: Materials
  - key: Copies
    value: "2"
  - key: Print technique (lettering)
    value: 3D
  - key: Size
    value: 30 x 90 cm
---
