---
title: opexp026
layout: experiment
number: 26
auteur:
  - name: Morgane Bartoli
    website: https://morganebartoli.com/
name: "Spécimen print Snap-it #2"
name-en: "Print Snap-it specimen #2"
date-str: 05/2019
thumbnail: /assets/uploads/opexp026-white.jpg
thumbnail-hover: /assets/uploads/opexp026-2.jpg
images:
  - /assets/uploads/opexp026-1.jpg
  - /assets/uploads/opexp026-2.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Flyer
  - key: Impression
    value: Numérique noir
  - key: Exemplaires
    value: 3 x 50
  - key: Papier
    value: Chromolux Alu Silver
  - key: Grammage
    value: "90"
  - key: Taille
    value: A6
exp-meta-en:
  - key: Format
    value: Flyer
  - key: Print technique
    value: Black digital print
  - key: Copies
    value: 3 x 50
  - key: Paper
    value: Chromolux Alu Silver
  - key: "Grammage "
    value: "90"
  - key: Size
    value: A6
---
