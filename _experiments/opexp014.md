---
title: opexp014
layout: experiment
number: 14
auteur:
  - name: "Morgane Bartoli "
    website: https://morganebartoli.com/
  - name: "Corentin Moussard "
    website: https://alune.fr/
name: "Teaser OP007 #2"
name-en: "OP007 teaser #2"
date-str: 02/2018
thumbnail: /assets/uploads/opexp014_white.jpg
thumbnail-hover: /assets/uploads/opexp014_black.jpg
video:
  has-video: true
  video: "434152434"
exp-meta-fr:
  - key: Format
    value: Vidéo
  - key: "Durée "
    value: 0:13’
  - key: Nombre
    value: "3"
  - key: Taille
    value: 1080 x 1920 px
exp-meta-en:
  - key: Format
    value: Video
  - key: Duration
    value: 0:13’
  - value: "3"
    key: Amount
  - key: Size
    value: 1080 x 1920 px
---
