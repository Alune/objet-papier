---
title: opexp013
layout: experiment
number: 13
auteur:
  - name: "Morgane Bartoli "
    website: https://morganebartoli.com/
  - name: "Corentin Moussard "
    website: https://alune.fr/
name: "Teaser OP007 #1"
name-en: "OP007 teaser #1"
date-str: 02/2018
thumbnail: /assets/uploads/opexp013-white0.jpg
thumbnail-hover: /assets/uploads/opexp013-black0.jpg
video:
  has-video: true
  video: "433790274"
exp-meta-fr:
  - key: Format
    value: Vidéo
  - key: Durée
    value: 0:22’
  - key: Nombre
    value: "1"
  - key: Taille
    value: 1200 x 630 px
exp-meta-en:
  - key: Format
    value: Video
  - key: "Duration "
    value: 0:22'
  - key: Amount
    value: "1"
  - key: Size
    value: 1200 x 630 px
---
