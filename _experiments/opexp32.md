---
title: opexp032
layout: experiment
number: 32
auteur:
  - name: Corentin Moussard
    website: https://alune.fr/
name: Page d'accueil OP008
name-en: "OP008 landing page "
date-str: 01/2020
thumbnail: /assets/uploads/opexp032-white.jpg
thumbnail-hover: /assets/uploads/opexp032-black.jpg
video:
  has-video: true
  video: "434156911"
exp-meta-fr:
  - key: Format
    value: Page d'accueil
  - key: Taille
    value: Responsive
exp-meta-en:
  - key: "Format "
    value: Landing page
  - key: Size
    value: Responsive
---
