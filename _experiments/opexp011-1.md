---
title: opexp012
layout: experiment
number: 12
auteur:
  - name: Juliette Bernachot
  - name: Mamzelle Heing
    website: https://heingshop.com/
  - name: Morgane Bartoli
    website: https://morganebartoli.com/
  - name: Ronan Deshaies
    website: https://cargocollective.com/ronandeshaies
name: Lancement OP006
name-en: OP006 launch party
date-str: 28/09/2018 — 27/10/2018
thumbnail: /assets/uploads/opexp012-0.jpg
images:
  - /assets/uploads/opexp012-5.jpg
  - /assets/uploads/opexp012-4.jpg
  - /assets/uploads/opexp012-3.jpg
  - /assets/uploads/opexp012-2.jpg
  - /assets/uploads/opexp012-1.jpg
  - /assets/uploads/opexp012-0.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: |-
      Vernissage + exposition\
      Vente de l’édition et d’illustrations originales
  - key: Lieu
    value: La Karambole, Paris 18
  - key: "Durée "
    value: Une soirée + un mois
exp-meta-en:
  - key: Format
    value: |-
      Launch party + exhibition\
      Sale of the edition and original illustrations
  - key: Duration
    value: One evening + one month
  - key: Location
    value: La Karambole, Paris 18
---
