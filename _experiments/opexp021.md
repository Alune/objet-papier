---
title: opexp021
layout: experiment
number: 21
auteur:
  - name: "Morgane Bartoli "
    website: https://morganebartoli.com/
  - name: "Corentin Moussard "
    website: https://alune.fr/
name: Site Print-it
name-en: Print-it website
date-str: 03/2019
thumbnail: /assets/uploads/opexp021-white.jpg
thumbnail-hover: /assets/uploads/opexp021.jpg
images:
  - /assets/uploads/opexp021.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Site web
  - key: Lien
    value: "[print-it.objetpapier.fr](https://print-it.objetpapier.fr/)"
  - key: Taille
    value: Responsive
exp-meta-en:
  - key: Format
    value: Website
  - value: "[print-it.objetpapier.fr](https://print-it.objetpapier.fr/)"
    key: Link
  - key: Size
    value: Responsive
---
