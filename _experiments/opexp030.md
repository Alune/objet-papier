---
title: opexp030
layout: experiment
number: 30
auteur:
  - name: Ronan Deshaies
    website: https://cargocollective.com/ronandeshaies
name: Péloche de tournée
date-str: 03/2019 — 03/2020
thumbnail: /assets/uploads/opexp30-8.jpg
images:
  - /assets/uploads/opexp30-1.jpg
  - /assets/uploads/opexp30-2.jpg
  - /assets/uploads/opexp30-3.jpg
  - /assets/uploads/opexp30-4.jpg
  - /assets/uploads/opexp30-5.jpg
  - /assets/uploads/opexp30-6.jpg
  - /assets/uploads/opexp30-7.jpg
  - /assets/uploads/opexp30-9.jpg
  - /assets/uploads/opexp30-10.jpg
  - /assets/uploads/opexp30-11.jpg
  - /assets/uploads/opexp30-12.jpg
  - /assets/uploads/opexp30-13.jpg
  - /assets/uploads/opexp30-14.jpg
  - /assets/uploads/opexp30-15.jpg
  - /assets/uploads/opexp30-16.jpg
  - /assets/uploads/opexp30-17.jpg
  - /assets/uploads/opexp30-18.jpg
  - /assets/uploads/opexp30-19.jpg
  - /assets/uploads/opexp30-20.jpg
  - /assets/uploads/opexp30-21.jpg
  - /assets/uploads/opexp30-22.jpg
  - /assets/uploads/opexp30-8.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Photos argentique
  - key: Nombre
    value: "22"
exp-meta-en:
  - key: Format
    value: Film photos
  - key: Amount
    value: "22"
---
