---
title: opexp007
layout: experiment
number: 7
auteur:
  - name: Ronan Deshaies
    website: https://cargocollective.com/ronandeshaies
  - name: Morgane Bartoli
    website: https://morganebartoli.com/
name: "J’ai scanné mon esprit "
date-str: 03/2018
thumbnail: /assets/uploads/opexp007-white.jpg
thumbnail-hover: /assets/uploads/opexp007-3.jpg
images:
  - /assets/uploads/opexp007-1.jpg
  - /assets/uploads/opexp007-2.jpg
  - /assets/uploads/opexp007-3.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Affiche
  - key: Impression
    value: "Risographie bichromie "
  - value: Maison Riso
    key: Imprimeur
  - key: Exemplaires
    value: "50"
  - key: Distribution
    value: "[Riso Friends](http://risofriends.com/)"
  - key: Taille
    value: A3
exp-meta-en:
  - key: Format
    value: Poster
  - key: Print technique
    value: "Risography 2 colors "
  - value: Maison Riso
    key: Printer
  - key: Copies
    value: "50"
  - key: Distribution
    value: "[Riso Friends](http://risofriends.com/)"
  - key: Size
    value: A3
---
