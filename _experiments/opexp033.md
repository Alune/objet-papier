---
title: opexp033
layout: experiment
number: 33
auteur:
  - name: Ronan Deshaies
    website: https://cargocollective.com/ronandeshaies
name: Ligne de fuite
date-str: 11/2020
thumbnail: /assets/uploads/opexp033-white.jpg
thumbnail-hover: /assets/uploads/opexp033-black.jpg
images:
  - /assets/uploads/opexp033-8.jpg
  - /assets/uploads/opexp033-5.jpg
  - /assets/uploads/opexp033-2.jpg
  - /assets/uploads/opexp033-9.jpg
  - /assets/uploads/opexp033-3.jpg
  - /assets/uploads/opexp033-4.jpg
  - /assets/uploads/opexp033-6.jpg
  - /assets/uploads/opexp033-7.jpg
  - /assets/uploads/opexp033-0.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Plans de métro parisien
  - key: Technique
    value: Peinture
  - key: Propos
    value: >-
      (Tentative d’évasion de la sphère parisienne.)

      « La mortalité, c'est se mouvoir en ligne droite dans un univers où rien ne bouge, si ce n'est en cercle. » Hannah Arendt, Condition de l'homme moderne (1958)
exp-meta-en:
  - key: Format
    value: Parisian subway map
  - key: Technique
    value: Painting
  - key: About
    value: (Attempt to escape from the Parisian sphere.) “Mortality is to move in a
      straight line in a universe where nothing moves, other than in a circle. »
      Hannah Arendt, The Human Condition (1958)
---
