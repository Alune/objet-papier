---
title: opexp023
layout: experiment
number: 23
auteur:
  - name: "Morgane Bartoli "
    website: https://morganebartoli.com/
  - name: "Corentin Moussard "
    website: https://alune.fr/
name: "Font Snap-it regular "
name-en: "Snap-it regular typeface "
date-str: 03/2019
thumbnail: /assets/uploads/opexp023-white.jpg
thumbnail-hover: /assets/uploads/opexp023-1.jpg
images:
  - /assets/uploads/opexp023-4.jpg
  - /assets/uploads/opexp023-3.jpg
  - /assets/uploads/opexp023-2.jpg
  - /assets/uploads/opexp023-1.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Police de caractère
  - key: Licence
    value: OFL
  - key: "Lien "
    value: "[gitlab.com/Alune/snap-it-mono-font](https://gitlab.com/Alune/snap-it-m\
      ono-font)"
exp-meta-en:
  - key: Format
    value: Type font
  - key: License
    value: OFL
  - key: link
    value: "[gitlab.com/Alune/snap-it-mono-font](https://gitlab.com/Alune/snap-it-m\
      ono-font)"
---
