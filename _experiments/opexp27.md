---
title: opexp027
layout: experiment
number: 27
auteur:
  - name: Morgane Bartoli
    website: https://morganebartoli.com/
name: Spécimen Tattoo Snap-it
name-en: "Snap-it Tattoo Specimen "
date-str: 05/2019
thumbnail: /assets/uploads/opexp027-white.jpg
thumbnail-hover: /assets/uploads/opexp027.jpg
images:
  - /assets/uploads/opexp027.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Planche de tatouage
  - key: Exemplaires
    value: "20"
  - key: Taille
    value: A5
exp-meta-en:
  - key: Format
    value: Tattoo board
  - key: Copies
    value: "20"
  - key: Size
    value: A5
---
