---
title: opexp006
layout: experiment
number: 6
auteur:
  - name: Ronan Deshaies
    website: https://cargocollective.com/ronandeshaies
name: "Séance de travail "
date-str: 10/2017
thumbnail: /assets/uploads/opexp006-white.jpg
thumbnail-hover: /assets/uploads/opexp006.jpg
images:
  - /assets/uploads/opexp006.jpg
video:
  has-video: false
exp-meta-fr:
  - key: Format
    value: Flyer
  - key: Impression
    value: Numérique
  - key: Façonnage
    value: Pliage et découpage manuel
  - key: Exemplaires
    value: "50"
  - key: Taille
    value: |-
      11 x 7,75 cm (fermé) \
      11 x 31 cm (ouvert)
exp-meta-en:
  - value: Flyer
    key: Format
  - key: Print technique
    value: Digital
  - key: Binding
    value: Manual folding and cutting
  - key: Copies
    value: "50"
  - key: Size
    value: |-
      11 x 7.75 cm (closed)\
      11 x 31 cm (open)
---
